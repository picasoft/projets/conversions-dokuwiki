## Activation du hotplug des disques

Il est souvent nécessaire d'attacher un disque virtuel supplémentaire à une machine virtuelle (pour augmenter l'espace disponible ou utiliser un autre type de stockage).

Par défaut, quand on ajoute une ressource sur une machine virtuelle (CPU, RAM, disque...) via l'interface Proxmox ou en ligne de commande, il faut redémarrer la VM. Même chose quand on veut modifier la taille d'un disque.

Il est souhaitable de pouvoir changer cette configuration à chaud sans redémarrage des VM.

Pour ceci, on exploite la fonctionnalité "Hotplug" de Proxmox, et les fonctionnalités "Hotplug" des noyaux Linux récents.

<bootnote>Ici, on ne traite que du hotplug des disques car on a a priori pas besoin de la fonctionnalité au niveau CPU et mémoire qui requiert [NUMA](https://fr.wikipedia.org/wiki/Non_uniform_memory_access), que je ne maîtrise pas.</bootnote>

<bootnote warning>La méthode ne fonctionne pas avec les disques IDE/SATA, uniquement SCSI/VirtIO.</bootnote>

### Sur la machine virtuelle

Sur les noyaux récents, il n'y a pas besoin de charger des modules spécifiques, les fonctionnalités sont probablement déjà activées dans le kernel.

On vérifie comme ceci :

```bash
$ cat /boot/config-$(uname -r) | grep -i hotplug
# Les lignes suivantes doivent être présentes
CONFIG_MEMORY_HOTPLUG=y
CONFIG_HOTPLUG_PCI=y
CONFIG_HOTPLUG_PCI_ACPI=y
```

Si les lignes ne sont pas présentes, on chargera les modules suivants :
```bash
$ modprobe acpiphp
$ modprobe pci_hotplug
```

Si cela fonctionne, on les charge automatiquement au démarrage :
```bash
$ echo acpiphp >> /etc/modules
$ echo pci_hotplug >> /etc/modules
```

### Sur l'hôte

<bootnote>Depuis Proxmox 6, l'option est accessible depuis l'interface graphique :
{{ :technique:infrastructure:machines_virtuelles:proxmox_6_hotplug.png |}}</bootnote>

Sur l'hôte, on se rend dans le dossier `/etc/pve/qemu-server`, et on édite le fichier de configuration correspondant à la VM (la correspondance entre numéro et nom de machine) peut se retrouve dans l'interface ou le fichier de configuration lui-même.

Dans `<id>.conf`, on ajoutera la ligne suivante :

```
hotplug: disk
```

### Vérification et troubleshooting

On redémarre la machine virtuelle depuis l'interface Proxmox et on ajoute un disque dur dans la VM.

Si ce disque apparaît en rouge, cela veut dire que le hotplug n'a pas fonctionné. On peut aussi vérifier que ça n'a pas fonctionné en observant l'absence de nouvelle entrée dans `dmesg -w` et `fdisk -l` sur la machine virtuelle.

Dans ce cas, on peut tout de même exploiter l'hotplug en attachant manuellement le disque à la machine virtuelle.
Il faut d'abord trouver le chemin de l'image du disque virtuel, par exemple :

```bash
# local:102/vm-102-disk-0.qcow2 est le nom du disque indiqué
# dans l'interface web Proxmox au niveau de la VM
$ pvesm path local:102/vm-102-disk-0.qcow2
/var/lib/vz/images/102/vm-102-disk-0.qcow2
```

Pour ce faire, sur l'hôte :

```bash
# 102 est l'identifiant de la VM
$ qm monitor 102
qm> pci_add auto storage file=<chemin>,if=[scsi|virtio]
```