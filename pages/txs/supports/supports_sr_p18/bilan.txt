====== Ce que m'a apporté cette TX ======

===== Marc =====

Tout d'abord, on acquiert de la connaissance à propos de technologies qu'on ne connait pas et qu'on ne découvrira surement pas à l'UTC. Je ne vous ferai pas le plaisir d'énumérer les technologies que j'ai personnellement découvertes; je vous redirige plutôt vers les [[txs:supports:supports_sr_p18:decoupage|modules que j'ai produit]]. De plus, vous découvrez une activité qui vous est sûrement étrangère : la création de cours. Cela est bien plus complexe qu'il n'y parait si on souhaite transmettre efficacement des connaissances. Enfin, on découvre beaucoup de méthodes. D'une part, cela m'a fait prendre goût à la méthode agile et son efficacité. D'autre part, je me suis familiarisé avec la documentation officielle qui est souvent un peu effrayante lorsqu'on y est pas habitué.

En résumé, cette TX conclue parfaitement mon TC et me permettra d'aborder mon GI avec beaucoup de confiance et de connaissances.
Un grand merci à Antoine et Stéphane.

===== Alexis =====

Comme je l'espérais au début de cette TX, j'ai appris. J'ai appris de la technique, et notamment sur Docker, une technologie que je voulais aborder depuis longtemps. Du côté humain, j'en retire du positif même s'il est vrai qu'elle aurait pu mieux se dérouler (notamment par un travail plus régulier de ma part). Elle m'a également beaucoup appris sur la gestion de projet, la nécessité de s'adapter à de nouvelles méthodes, des relations humaines au sein d'une équipe et de son organisation agile. Cette TX m'a fait me poser beaucoup de questions, et je suis convaincu que cette expérience m'aura apporté beaucoup d'éléments de compréhension du monde du travail. 

Une autre chose de positif, qui est ressortie avec l'organisation Docker notamment : les UTCéens ont //besoin// de ce genre de formation. Je suis fier d'avoir pu proposer cet atelier à non pas 5 copains (comme je me le serais imaginé au départ), mais à 30 étudiants qui sont reparti avec quelque chose qui leur sera utile. Je ne m'attendais pas à un tel engouement et ceci est à mon sens révélateur d'une volonté d'évolution du GI.
Bien que l'étude des fondamentaux doive à mon sens rester au premier plan, je trouve qu'il est intéressant de donner aux UTCéens la possibilité de se pencher sur ces technologies, notions d'actualité qui pourront les aider à rentrer plus facilement sur monde du marché, mais aussi à être formé sur les outils les plus utilisés dans un contexte non universitaire.

Je remercie Antoine et Stéphane de m'avoir permis de suivre cette TX ainsi que du temps qu'ils ont pu me consacrer durant ce semestre.