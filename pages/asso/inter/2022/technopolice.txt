## Conférence Technopolice

<bootnote>Archive du pad avec tous les textes : {{ :asso:inter:2022:plan-comm-technopolice-24-05.odt |}}</bootnote>

Retour rapide sur la conférence Technopolice présentée par LQDN en mai 2022. Elle a réunit près de 70 personnes. Le sujet a largement participé de son succès, mais un gros effort de communication a été fait. Ce "plan" pourra être ré-utilisé pour de prochaines conférences.

Il est en deux parties :

* Un teasing, visible et volontairement flou, qui suscite la curiosité et pose une date. 
* Une annonce, sur tous les canaux possibles, pour "révéler" l'événement.

### Teasing

S'y prendre trois semaines avant. Diffuser deux semaines avant.

* Des affiches A3 partout dans BF et un post sur les réseaux sociaux avec un texte un peu fiction. Pour voir l'esprit :
{{ :asso:inter:2022:teaser-a4_v2.jpeg?400 |}}

* Une version "réseaux sociaux" de cette affiche (donc horizontale) sur le profil Facebook de Picasoft, sur UTC =) et sur Mastodon.
* Des posts interrogateurs sur Facebook depuis des comptes de membres pour stimuler les réactions.

### Annonce

S'y prendre deux semaines avant. Diffuser une semaine avant.

* Spot pub sur Graf'hit : il faut contacter Leo <leo@graphit.net>. Le mieux est d'arriver avec un texte et les éléments à rajouter au montage. Exemple du spot qui a tourné pendant une semaine : {{ :asso:inter:2022:202205_spot_conf_technopolice.wav | Fichier WAV}} 

* Créer un événement Mobilizon pour centraliser les infos. Archive de l'événement : https://mobilizon.picasoft.net/events/f4738be8-1475-4a26-beab-d20f7654dcf7
* Des affiches A3 partout dans BF qui remplacent les précédentes. Exemple (adapté aux réseaux sociaux ici) :
{{ :asso:inter:2022:index.png?400 |}}

* Poster les annonces sur les réseaux sociaux en adaptant le texte pour chacun : UTC =), Mastodon. 
* Diffuser sur l'Agenda du Libre
* Post sur le portail des assos
* Messages sur Mattermost :
  * Sur l'équipe Générale de Picasoft en @all
  * Sur l'équipe Hors-Sujet d'équipes sympathisantes (CeT, Convergence, Préau, Café des Lumières, ID en UT)
* Mail sur la liste `picasoft-enthousiastes`
* Message dans Actualités-UTC et sur le compte Facebook de l'UTC
* Weekpost du BDE

Deux/trois jours avant :

* Poser des flyers sur les tables des gros amphis de BF (s'y prendre plutôt tôt le matin que tard le soir pour éviter le nettoyage)
* Tracter sur la passerelle à midi
* Poser dans les amphis/salles de TD juste avant certains cours en lien avec la conf.

