{{indexmenu_n>2}}
# Gestion des beats

Les beats sont un élément essentiel de la solution de monitoring proposé ici. 

Un Beat est un collecteur déployés sur les sources de données. Parmi les principaux :

* //metricbeat// : mesure les performances systèmes telles que mémoire et CPU. Mais également l’espace disque, les processus… Il incorpore une série de modules plus précis : pour Docker, MySQL, PostgreSQL… Dans le cadre de l'infrastructure de Picasoft on utilise metricbeat pour les metrics Docker et celles de la machine hôte.


* //packetbeat// : extraction de données depuis une analyse de paquets réseaux. Equivalent (TCP dump + wireshark.)

`WARNING`: packetbeat n'est pas utilisé dans le cadre de Picasoft.

* //filebeat// : Filebeat surveille les fichiers de log. C’est tout. Mais il le fait bien : reprise en cas d’interruption, pré-filtrage avant d’envoyer les lignes, ajout de champs et d’étiquettes dès la source… Par contre, il faudra prendre le temps de maîtriser Logstash et son plugin Grok pour ne pas tout saturer dans votre cluster Elasticsearch. Si on ne souhaite pas passer par logstash il faut passer par un pipeline.
[[https://www.elastic.co/guide/en/logstash/current/use-ingest-pipelines.html|Doc ingest pipeline]]

`WARNING`: filebeat n'est pas utilisé dans le cadre de Picasoft (pour le moment).

L’intérêt de la solution est ici de répartir les beats sur les machines distante en fonction des besoins.

{{ :technique:monitoring:monitoring_es:beats-platform.png?600 |}}

C'est donc la machine "centrale" monitoring qui possède kibana et elasticsearch. Les beats sont eux déployé sur toute les machines où l'on souhaite faire du monitoring. On va donc déployer metricbeat sur toutes les machines de Picasoft. 

__Evolution possible:__
Si un service en particulier a besoin d'être monitoré/alerté on peut déployer filebeat sur la machine qui héberge ce service et le brancher via un pipeline au service. Elasticsearch récupère ainsi en remote les logs de ce service et on peut les traiter à l'aide de la solution. 

`WARNING`:
Nous déconseillons d'utiliser logstash si ce n'est pour un besoin très précis et établi à l'avance. Pour rappel, logstash permet de collecter les logs provenant de différents beats (agents collecteurs) de les analyser avant de les stocker. On qualifie logstash de __ETL__ pour Extract-Transform-Load. Le soucis de logstash est que pour "trier" les logs à ajouter à elasticsearch, on passe souvent par un systeme de filtre appellé "grok". **Grok** est un plugin logstash permettant de parser les logs avant de les indexer. Le soucis est que la syntaxe n'est pas du tout intuitive. Il est alors très difficile à maintenir et peu évolutif. D'autres travaux à Picasoft font état d'un constat similaire. Il s'agit ici d'un avertissement et non d'une interdiction sur l'utilisation de logstash.
Si logstash doit ici être utilisé nous recommandons l'utilisation d'[ingest pipeline](https://www.elastic.co/guide/en/logstash/current/use-ingest-pipelines.html). Tout comme les groks, les pipelines permettent de parser les datas avant de les indexer dans elasticsearch. 
*Un cas d'usage serait de ne sélectionner que les logs prevenant d'un service particulier pour un filebeat déployé sur une machine donnée.* 
