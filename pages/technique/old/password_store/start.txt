## Gestion des mots de passe

<bootnote web>Les identifiants de l'association (mots de passes, clés, etc) sont stockés sur [ce dépôt Gitlab](https://gitlab.utc.fr/picasoft/interne/pass). Le dépôt est privé, tu peux demander l'accès à un membre!</bootnote>

<bootnote critical>Tu arrives sur cette page, tu as juste besoin d'un mot de passe et tu ne comprends rien à ces pages ? Demande à un membre de l'équipe tech et on fera ensemble ! :-D</bootnote>

### Des mots de passe sur Gitlab ?

Les mots de passe sont chiffrés avec les clés PGP des membres autorisés.

Pour gérer les identifiants, nous utilisons [pass](https://www.passwordstore.org/), qui permet une gestion simple de ces mots de passe en ligne de commande. Il a de nombreux clients (extension de navigateur, application mobile, menu intégré à l'environnement graphique...).

{{indexmenu>.|js#bitrix msort tsort nsort notoc nomenu nocookie navbar}}