## Demander une facture à Tetaneutral

<bootnote warning>Cette page est un copier/coller de [ce post](https://team.picasoft.net/picasoft/pl/aork69wisf8gbc4ktexpc7upyw) pour retrouver des infos plus rapidement. Elle est à faire évoluer ! :-)</bootnote>


Nous louons l'hébergement physique des machines [[technique:infrastructure:config|Alice et Bob chez le FAI associatif Tetaneutral]] à Toulouse ainsi que plusieurs adresses IP, comme [détaillé ici](https://wiki.picasoft.net/doku.php?id=asso:treso:suivi-couts#hebergement). Nous les payons annuellement, **en début d'année**. Ceci étant, ce n'est pas l'activité principale de l'asso et iels oublient chaque année de nous envoyer la facture, probablement par manque d'effectif. Nous prenons donc l'habitude de leur demander une facture, par honnêteté : il s'agit d'une asso comme nous, pas d'une grosse structure pleine de fric.

Pour l'heure, la manière la plus rapide d'obtenir une réponse est d'envoyer un mail simultanément à :

- Équipage Banque Tetaneutral.net <equipage-banque@lists.tetaneutral.net>
- Sylvain Revault <sylvain.revault@gmail.com>
- Élie Bouttier <elie+ttnn@bouttier.eu>

La facture se décompose en trois partie :
- Une part fixe d'adhésion à Tetaneutral
- Une part fixe d'hébergement des machines
- Un prix libre pour les IP : nous avons par le passé considéré que 2€/mois/IP était juste.

Pour leur faciliter la tâche, notre numéro d'adhésion en interne est le suivant : **ADT991**