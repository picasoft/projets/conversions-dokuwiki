#!/bin/env bash

# finir de nettoyer les fichier
sanitize() {
	if [ -z "$1" ]
	then
		echo "Un fichier doit être indiqué"
		return 1
	fi
	# transformer les liens restants en markdown
	sed -i "s/\[\[\([^|]*\)|\([^]]*\)\]\]/[\2](\1)/g" $1
	# enlever les bootnote
	sed -i "s/<\/*bootnote[[:space:][:alpha:]]*>//g" $1
	# changer les images
	sed -i "s/{{\([^|]+\.(png|jpg|svg|jpeg|bmp)(\?[^|]*)?\)|\([^}]*\)}}/![\2](\1)/g" $1
	# changer les autres fichiers joints
	sed -i "s/{{\([^|]+\.(zip|tar|tex|pdf|tgz|tar\.gz)\)|\([^}]*\)}}/[\2](\1)/g" $1
	# supprimer diverses lignes spécifiques à dokuwiki, table des matières, lignes de contrôle des plugins, ...
	sed -i "/^~~NOTOC~~$/d" $1
	sed -i "/^{{[^}]*}}$/d" $1
}

# converties est notre répertoire de travail
rm -rf converties
cp -r pages converties

# on considère comme non markdown les fichier ne contenant aucun titre en markdown (quel que soit le niveau du titre)
# conversion des fichiers au format dokuwiki vers du markdown
for fichier in $(grep -rLE "^#+ " converties)
do
	echo "-> Conversion de $fichier"
	nouveaunom="${fichier%.txt}.md"
	pandoc --from=dokuwiki "$fichier" --to markdown -o "$nouveaunom"
	sanitize "$nouveaunom"
	rm "$fichier"
done

# les autres fichiers sont déjà en markdown, on remplace juste l'extension par du md
for fichier in $(grep -rlE "^#+ " converties)
do
	echo "-> Renommage de $fichier"
	nouveaunom="${fichier%.txt}.md"
	mv -- "$fichier" "$nouveaunom"
	sanitize "$nouveaunom"
done
