## Etherpad

Cette section comprend les informations utiles pour administrer Etherpad.

<bootnote>Etherpad est un éditeur de texte libre en ligne. Il permet la rédaction de document en mode collaboratif et en temps réel par plusieurs personnes. L'ajout de commentaires, une messagerie instantanée et le versionnage de document sont aussi possibles.

Pour Picasoft, Etherpad est l'outil utilisé pour la rédaction des comptes rendus de réunions ou de simples notes ou d'idées.</bootnote>

<bootnote web>Les principales informations sur Etherpad se trouvent [sur le wiki](https://github.com/ether/etherpad-lite/wiki) et sur la [documentation officielle](https://etherpad.org/doc/v1.8.6/) (remplacer la version dans l'URL par la version courante). Cette section résume certaines opérations courantes.</bootnote>

Etherpad est déployé et configuré à partir de ces fichiers ([instance annuelle](https://gitlab.utc.fr/picasoft/projets/services/etherpad-yearly), [instance bimensuelle](https://gitlab.utc.fr/picasoft/projets/services/etherpad-weekly)) qui complètent cette documentation.

{{indexmenu>:technique:adminserv:etherpad|js#bitrix msort tsort nsort notoc nomenu nocookie navbar}}