====== TX "Support SR" ======

====Introduction====

L'objectif de notre TX est la participation à la conception d'un support d'apprentissage en autonomie "Introduction aux systèmes et réseaux". Le but de ce support est de permettre aux étudiants de se former en autonomie sur divers thèmes liés au réseau.

Le sujet s'oriente autour des points suivants: 
  - Installation et configuration d'un serveur Linux/Debian
  - Configuration réseau
  - Déploiement d'une architecture LNPP


La présentation de notre travail s'articulera de la façon suivante: 
  - [[txs:supports:supports_sr_a17:organisation|Organisation]]
  - [[txs:supports:supports_sr_a17:methodologie|Méthodologie]]
  - [[txs:supports:supports_sr_a17:outils|Outils utilisés]]
  - [[txs:supports:supports_sr_a17:decoupage|Découpage des modules]]
  - [[txs:supports:supports_sr_a17:test|Séances de tests et retours]]
  - [[txs:supports:supports_sr_a17:bilan|Bilan moral et technique]]
  - [[txs:supports:supports_sr_a17:conseils|Conseils pour les futurs étudiants]]
  - [[txs:supports:supports_sr_a17:idees|Idées/pistes pour prochains modules]]
  - [[txs:supports:supports_sr_a17:annexe|Sources et annexes]] 