{{indexmenu_n>20}}

# Centraliser les journaux 

Dans le cadre de la TX Sécurité P19, nous avons mis en place un mécanisme de centralisation des logs produits par le démon d'audit `auditd`.

## Objectif et approche utilisée

Il est très important que les journaux soient centralisés car ils seront
principalement utilisés pour des analyses post-mortem. Or, on n'a pas de
garantie d'intégrité sur les journaux d'un hôte compromis, donc on souhaite les
conserver sur un hôte plus sûr (`monitoring`).

auditd permet d'utiliser Kerberos pour cela, mais cette solution est très
lourde, compliquée à mettre en place et à maintenir. Nous avons plutôt décidé
d'utiliser `rsyslog` car il est déjà présent sur tous les hôtes et permettra de
centraliser d'autres journaux que ceux produits par auditd. Il s'agit d'une
solution mature et très capable, permettant notamment de discriminer les
journaux en fonction de leur hôte d'origine.

La mise en place de ce système se fait en plusieurs étapes : il faut rediriger
les journaux d'auditd vers rsyslog, configurer rsyslog pour qu'il envoie ses
journaux sur `monitoring` et configurer `monitoring` pour qu'il gère
correctement ces journaux et les tourne de façon appropriée.

**A terme, l'authentification TLS devra être activée. Un exemple de mise
en place de celle-ci peut être trouvée [ici](https://www.rsyslog.com/using-tls-with-relp/).**

## Configuration
### Machines clientes
#### Dispatcher d'auditd :
On commence par appliquer la configuration suivante à
`/etc/audisp/plugins.d/syslog.conf` sur chacun des clients:

```
active = yes
direction = out
path = builtin_syslog
type = builtin
args = LOG_INFO
format = string
```
Cela active le plugin `audisp` passant les journaux d'auditd à rsyslog sur
chaque client. 

#### rsyslog sur les clients :
On configure ensuite rsyslog dans `/etc/rsyslog.d/client.conf`
pour qu'il envoie ses journaux à `monitoring` :


```
###########################################
# Log centralization client configuration #
###########################################
module(load="omrelp")   # provides RELP output for log centralization

# Forward auditd and audispd logs to a distant host
if ($programname == "auditd" or $programname == "audispd") then {
    action(type="omrelp"
        target="monitoring.picasoft.net"
        port="20514"
        tls="on"
        # For tls authentication
    #     tls.caCert="/path/to/cert"
    #     tls.myCert="/path/to/cert"
    #     tls.myPrivKey="/path/to/key"
    #     tls.authmode="name"
    #     tls.permittedpeer=["monitoring.picasoft.net"]
)
}
```

On remarquera que l'authentification TLS n'a pas encore été mise en place car
nous attendions encore les certificats nécessaires.

### Serveur de centralisation

On configure enfin `monitoring` pour qu'il gère correctement les journaux lui
étant envoyés.
#### rsyslog sur le serveur :
On commence par rajouter la configuration suivante à
`/etc/rsyslog.d/server.conf` pour que les journaux entrant soient ségrégés en
fonction de l'hôte dont ils proviennent :

```
module(load="imrelp" ruleset="relp")   # provides RELP input for log centralization
                                       # and bind it to ruleset relp (see below)
input(type="imrelp" port="20514"
    tls="on" 
# For tls authentication:
#     tls.caCert="/path/to/cert" 
#     tls.myCert="/path/to/cert" 
#     tls.myPrivKey="/path/to/key"
#     tls.authMode="name" 
#     tls.permittedpeer=["pica02.picasoft.net", "pica01.picasoft.net", "pica01-test.picasoft.net"]
) 

# Places the right log in the right file
ruleset (name="relp") {
    if $source == "pica01-test" then {
        action(type="omfile"
            file="/var/log/pica01-test.log") 
        stop
    }
    if $source == "pica01" then {
        action(type="omfile"
            file="/var/log/pica01.log") 
        stop
    }
    if $source == "pica02" then {
        action(type="omfile"
            file="/var/log/pica02.log") 
        stop
    }
    stop
}
```

#### logrotate sur le serveur :
Puis on rajoute la configuration suivant à `/etc/logrotate.conf` pour déterminer
le comportement des rotations :

```
/var/log/pica01-test.log
/var/log/pica01.log
/var/log/pica02.log
{
    # keep log files for 30 days
    maxage 30
    # Rotate them when they are larger than 10M
    size 10M
    missingok
    notifempty
    compress
    delaycompress
    sharedscripts
    postrotate
            invoke-rc.d rsyslog rotate > /dev/null
    endscript
}
```

Cette configuration supprime les journaux plus vieux qu'un mois, car on suppose
qu'un problème nécessitant une enquête sera découvert avant cela. Par ailleurs,
ne pas supprimer les journaux lorsqu'ils occupent une taille trop importante
permet d'éviter qu'un attaquant les supprime en effectuant de nombreux appels à
`execve` depuis un shell.
Nous avons ajouté cette portion directement dans le fichier `logrotate.conf` car
c'est la méthode indiquée comme privilégiée dans la documentation de cet outil.

## Extension à d'autres services
Grâce à son approche modulaire, il est relativement facile d'utiliser `rsyslog`
pour centraliser des logs produits par une grande diversité de services.

<bootnote>
Il semble que les personnes travaillant sur ce sujet avaient fait face à des
difficultés pour discriminer les logs en fonction du conteneur docker les ayant
produit. Il serait intéressant de reprendre leurs recherche pour avoir à
disposition une solution de centralisation globale.
</bootnote>