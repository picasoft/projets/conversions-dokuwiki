{{indexmenu_n>20}}
# Installation du serveur DNS principal

On va commencer par mettre en place un serveur DNS de référence pour Picasoft. Celui-ci se trouve, pour le moment, sur Alice.
<bootnote warning>Cette configuration n'est qu'à faire une seule fois!</bootnote>

## Préambule

<bootnote>J'utilise la terminologie *principal/secondaire* au lieu de *maître/esclave* quand le contexte le permet.</bootnote>

Nous utilisons le logiciel [[https://fr.wikipedia.org/wiki/BIND|BIND]] comme serveur DNS.

<bootnote>BIND est un serveur DNS de référence. Il est très puissant. En revanche, sa configuration est assez pénible à prendre en main. Des alternatives modernes, plus facilement configurables et tout aussi efficaces existent, comme [Knot DNS](https://en.wikipedia.org/wiki/Knot_DNS). Nous pourrions l'utiliser dans le futur et en profiter pour mettre en place [DNSSEC](https://docs.gandi.net/fr/noms_domaine/utilisateurs_avances/dnssec.html#comment-installer-dnssec-sur-votre-nom-de-domaine-avec-des-serveurs-dns-externes), la procédure étant largement simplifiée grâce à Knot.</bootnote>

<bootnote learn>La notion de DNS principal est purement interne à la configuration de BIND. De l'extérieur, les serveurs sont indifférenciés. La configuration principal/secondaire permet de modifier les enregistrements DNS sur le principal, et d'appliquer les changements aux secondaires automatiquement.</bootnote>

Toutes les commandes sont à faire en `root`.

## Installation de BIND

Il suffit d'un simple

```
apt install bind9
```

## Configuration

Les fichiers de configuration de BIND sont versionnés sur [ce dépôt](https://gitlab.utc.fr/picasoft/projets/zonefile-picasoft). Il est privé (on ne souhaite pas forcément que quelqu'un puisse avoir connaissance de tous nos sous-domaines).

On commence par cloner le dépôt dans `/etc/bind/picasoft` :

```
git clone https://gitlab.utc.fr/picasoft/projets/zonefile-picasoft /etc/bind/picasoft
```

On va ensuite configurer BIND pour utiliser ces fichiers de configuration. Dans `/etc/bind/named.conf`, on remplace le contenu du fichier par :

```
include "/etc/bind/named.conf.options";
include "/etc/bind/picasoft.net/named.conf.local";
include "/etc/bind/named.conf.default-zones";
```

Les fichiers de configuration de BIND par défaut conviennent très bien. Le seul fichier que l'on va modifier, versionné sur Git, est `named.conf.local`.

## Démarrer le serveur DNS

```
systemctl enable --now bind9
systemctl status bind9
```

On vérifie que les logs ne produisent aucune erreur.

On pourra vérifier que le serveur tourne bien en l'interrogeant depuis un poste client :

```
dig +short picasoft.net @<IP Alice>
```

Si on a une réponse, alors le serveur tourne correctement.

## Explication de configuration

L'idée n'est pas de documenter la syntaxe de configuration de BIND, mais voici un extrait possible du fichier `named.conf.local` pour comprendre :

```
zone "picasoft.net" {
        # Serveur principal
        type master;
        # Chemin du fichier de zone
        file "/etc/bind/picasoft.net/db.picasoft.net";
        # On autorise le transfert aux serveurs secondaires
        allow-transfer{91.224.148.85; 51.158.76.113;};
};
```

On dit à notre serveur BIND qu'il va être amené à gérer la zone `picasoft.net`, et que les **enregistrements DNS** se trouvent dans le fichier `db.picasoft.net`.

<bootnote learn>Un enregistrement DNS est composé d'un **type**, d'une **clé** et d'une **valeur**. Lorsque l'on fait une requête à un serveur DNS, on spécifie généralement le type et la clé. Par exemple : quel est la valeur de l'enregistrement de type `A` ayant la clé `team.picasoft.net` ? Sachant qu'un enregistrement de type `A` met en relation un nom de domaine et une adresse IP. Il en existe [beaucoup d'autres](https://fr.wikipedia.org/wiki/Liste_des_enregistrements_DNS), utiles pour les serveurs mails, les IPv6, les alias...</bootnote>

On indique ensuite les IP des serveurs secondaires, qui sont autorisés à recevoir l'intégralité des enregistrements DNS pour le domaine `picasoft.net`, communément appelée **zone**.