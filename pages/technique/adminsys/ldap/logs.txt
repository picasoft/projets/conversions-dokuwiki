{{indexmenu_n>7}}

## Vérifier les logs du serveur LDAP

Les logs se trouvent dans le conteneur, dans `/var/log.`

Un logrotate est installé pour ne pas surcharger le système, avec une configuration style :

```
cat /etc/logrotate.d/openldap
/var/log/*.log {
   missingok
   notifempty
   compress
   weekly
   rotate 10
   size=50M
   sharedscripts
   postrotate
 # OpenLDAP logs via syslog, restart syslog if running
   /etc/init.d/rsyslog restart
 endscript
}
```

C'est un peu bourrin puisque ça prend tous les logs, mais on est dans le conteneur.

<bootnote question>Comment changer le niveau de log ?</bootnote>

Il faut changer le paramètre `oclLogLevel` dans la [[technique:adminsys:ldap:utilisation##pour_modifier_la_configuration|configuration du serveur LDAP]].