# DKIM

Cette page concerne l'implémentation du protocole DKIM chez Picasoft. Pour une explication théorique de celui-ci, voir ce [[technique:old:etudes:mail:protocoles:smtp:limites#dkim|paragraphe]].

La configuration de DKIM pour Picasoft se fait en trois temps :
  - la génération d'une clef privée et d'une clef publique,
  - la publication de la clef publique dans le DNS,
  - la configuration de la signature automatique des mails sortant.

L'essentiel de ces opérations est assuré par des programmes développés par [[http://www.opendkim.org/|le projet OpenDKIM]]. Les paquets nécessaires sont ''opendkim'' et ''opendkim-tools'' sous Debian.

===== Génération des clefs =====

La génération des clefs s'effectue avec cette commande :
  opendkim-genkey -b 4096 -s nov2018 -d picasoft.net
L'option ''-b'' spécifie le nombre de bits de la clef (facultatif), ''-s'' correspond au sélecteur de la clef (une sorte de label)((Différentes conventions existent pour le format de ces sélecteurs (voir [[http://opendkim.org/opendkim-README|ici]]). Pour l'instant nous concaténons l'identificateur du mois et l'année de création, soit ''nov2018'' pour novembre 2018.)), ''-d'' est le nom de domaine. Cette commande va créer deux fichiers :
  * ''nov2018.private'' contenant la clef privée,
  * ''nov2018.txt'' contenant un enregistrement ''TXT'' avec la clef publique à copier/coller dans le DNS.

Ces deux fichiers sont enregistrés sous ''/DATA/docker/mail/opendkim/'', et la clef privée est montée dans le conteneur au lancement de celui-ci. Un fichier ''README.md'' est également présent dans ce répertoire pour aider l'administrateur et conserver un historique des clefs.

===== Publication de la clef publique dans le DNS =====

À titre d'exemple, le fichier ''nov2018.txt'' contient :
<code>
nov2018._domainkey	IN	TXT	( "v=DKIM1; h=sha256; k=rsa; "
	  "p=MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0LZPCh8GyDn1loMJs0IDfWVc6hJKnrDIQaUmcaBxtAoZUyEfTBdGIYaqswC/xCMa/ov01QCFP293RD1caN8h45CaArwoiPIjmM0xnq1YmSVRjABBozUVk6o8F08W9XzwO5iolWLfuMA+oiFnIyhH+ATcb8DxBvBTa8AUwCs6fEvfdSfIPlJNHhymWIbg2rWD01klOY36J0OZC1"
	  "p12Fl1fkUU5jNMcJZOezwrqbQqbqZQOF5+1BY/Wx2xWmjdlyn9OR4nE5046pF7l5/yOQr8EPVnrFK3dx2SNiJuOSj1qs56CAINkytvOoZEH65sULwh/HrQjvVvUYQjwYpVl3cOVLGIybo8n6ArsCUMcBiBjBHJRb6vUI7GM9gilZM17wOTDKL6ncxQPlya5DvmQCuf+2Kaz8HZAG/lAYHUqZ2+YaKDcR8k/xz9dQazqNqXSMvLn/DbisUD"
	  "d21vhfSJxEfW4FKQmJ6g9YSJCyzyKuvI8lyiwuXETM/dJI47Y3nlYJjJtIlxPn7MTvPyh5qByr1C3LrjEVFIe2dhA3C105PAkFMCpG7dut8l1nyxBUmJW1qmJ4dR/EHzwVfMnkcZPi/V8+6BUCT2hg95/rUTtwSD1JTTJduuvkAtrLSOaus35p0wsmmqU/glNDF3H9ARqJnAs/5UgEB+zOBs8fZuI/bcxCkCAwEAAQ==" )  ; ----- DKIM key nov2018 for picasoft.net
</code>
Il est rentré tel quel dans le DNS.
On peut aussi le retrouver avec la commande :
  dig -t txt nov2018._domainkey.picasoft.net
:!: Attention à bien penser à incrémenter le //serial// du DNS après modification de celui-ci.    

===== Configuration de OpenDKIM =====

La signature automatique des mails sortant est assurée par le paquet ''opendkim'', qui correspond au programme homonyme. Lors de l'installation de celui-ci, un fichier ''/etc/opendkim.conf'' est créé, ainsi qu'un utilisateur système et un groupe ''opendkim''.

Différents tuto existent sur le net, et présentent des pratiques différentes. Dans le cas de Picasoft, la configuration suivante((adaptée de cette page : [[https://www.postfix.io/how-to-configure-opendkim-with-postfix/]])) est adoptée :
  - la clef privée est montée dans le répertoire ''/etc/dkimkeys/'' sour le nom ''nov2018.picasoft.net.rsa'', (soit //selecteur//.//domaine//.rsa). **Cette tâche est réalisée par l'adminsys.**
  - les fichiers ''signingdomains.table'' et ''rsakeys.table'' présents dans ce répertoire permettent de dresser un tableau de clefs et faire des correspondance avec les noms de domaine (ne pas hésiter à les ouvrir pour comprendre leur structure), ils sont **générés automatiquement lors du run** par un script qui utilise les variables d'environnement docker My_DOMAIN et DKIM_SELECTOR
  - notre image docker met en place la config suivante:
    * on rajoute ces lignes au fichier ''opendkim.conf'' : <code>KeyTable /etc/dkimkeys/rsakeys.table
SigningTable refile:/etc/dkimkeys/signingdomains.table

# "simple" recommended by DKIMCore
Canonicalization simple

Mode sv
SubDomains no
AutoRestart yes
AutoRestartRate 10/1M
Background yes
DNSTimeout 5
SignatureAlgorithm rsa-sha256
</code>
    * on crée le répertoire où Postfix et OpenDKIM vont s'échanger se communiquer des //sockets// :<code>mkdir /var/spool/postfix/opendkim</code>
    * on modifie en conséquence ''/etc/default/opendkim'' et ''/etc/opendkim.conf'', respectivement : <code>SOCKET="local:/var/spool/postfix/opendkim/opendkim.sock"
Socket local:/var/spool/postfix/opendkim/opendkim.sock</code>
    * on rajoute ces lignes au fichier ''/etc/postfix/main.cf'':<code>milter_default_action = accept
milter_protocol = 6
smtpd_milters = local:/opendkim/opendkim.sock
non_smtpd_milters = local:/opendkim/opendkim.sock</code>
    * on met à jour les propriétaires : <code>chown -R opendkim:opendkim /etc/opendkim.conf /etc/dkimkeys
chown opendkim:postfix /var/spool/postfix/opendkim</code>
    * on rajoute l'utilisateur ''postfix'' au groupe ''opendkim' : <code>adduser postfix opendkim</code>
    * on relance OpenDKIM et Postfix.