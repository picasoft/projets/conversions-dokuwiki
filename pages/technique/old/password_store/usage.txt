{{indexmenu_n>20}}
## Utiliser pass au quotidien

<bootnote web>Première étape, cloner le dépôt où se trouvent les identifiants : https://gitlab.utc.fr/picasoft/interne/pass</bootnote>

### Manipuler les identifiants

<bootnote learn>
Le fonctionnement est ensuite exactement le même que pour `pass`,[voir le manuel](https://www.passwordstore.org/).
</bootnote>

#### Récupérer un mot de passe

Par exemple pour le mot de passe du compte `picasoft` sur notre instance Mobilizon :

```
picapass [-c] Asso/Moderation/mobilizon.picasoft.net/picasoft
```

Le switch `-c` permet de copier plutôt que d'afficher dans la console.

`picamenu` est beaucoup plus pratique, on peut filtrer en tapant un mot clé et le mot de passe est copié interactivement dans le presse papier.

<bootnote warning>`picamenu` est sensible à la casse!</bootnote>

#### Ajouter ou mettre à jour un mot de passe

On pourra générer un mot de passe et le chiffrer pour l'ensemble des personnes autorisées (voir [Ajouter une identité](#ajouter-une-identit%C3%A9)) du dossier avec la commande `picapass generate`. Exemple :

```bash
# Génère un mot de passe de 25 caractères pour le compte admin du Kanban
$ picapass generate Tech/kanban.picasoft.net/admin -n 25
```

Si en revanche on souhaite insérer un mot de passe déjà existant, ou le mettre à jour :

```bash
# Demande le mot de passe sur l'entrée standard
$ picapass insert Tech/WeKan/admin
```

La gestion avec Git est automatique : dès qu'une modification est faite avec `picapass`, un commit est effectué. 

<bootnote warning>Il ne faut pas oublier de `push` ses modifications.</bootnote>

### Alias pour le store Picasoft

`pass` ne propose par défaut qu'un seul « store », et cette situation est gênante quand vous gérez aussi vos mots de passe personnels.

La variable `$PASSWORD_STORE_DIR` permet de modifier le chemin vers le store concerné, il suffit de la mettre à jour avant de lancer la commande.

Les scripts `picapass` et `picamenu` sont des *wrappers* autour de la commande `pass` et `passmenu` (dépendance : `dmenu`), se contentant d'affecter automatiquement la variable `$PASSWORD_STORE_DIR`.

<bootnote>Utilisez donc `picapass` et `picamenu` exactement comme `pass` et `passmenu`.</bootnote>

Exemples :
```bash
# Copie dans le presse papier le mot de passe administrateur du cloud
$ picapass -c Cloud/admin
# Ouvre un menu graphique où l'on peut sélectionner le mot de passe à déchiffre et copier
$ picamenu
```

Pour plus de facilités, ces scripts peuvent être mis dans le `$PATH` ou ajoutés comme des liens symboliques dans un dossier du `$PATH`, par exemple :

```bash
# Méthode 1 : ajout au $PATH, dans ~/.bashrc, ou ~/.zshrc, ou...
export $PATH=${PATH}:/chemin/vers/le/clone/du/repo

# Méthode 2 : liens symboliques
$ cd /chemin/vers/le/clone/du/repo
$ sudo ln -s $(pwd)/picamenu /usr/local/bin/picamenu
$ sudo ln -s $(pwd)/picapass /usr/local/bin/picapass
```

Les scripts sont ensuite utilisables depuis un terminal ou un menu, de n'importe où.

### Importer les clés publiques des autres membres

À chaque fois qu'un mot de passe est créé ou mis à jour, il est chiffré avec les clés publiques des membres autorisé.e.s. Il vous faudra donc importer quelques clés publiques. Pour importer toutes les clés, vous pouvez utiliser le script [picaimport](https://gitlab.utc.fr/picasoft/interne/pass/-/blob/master/picaimport).

Ce script importe les clés et effectue une signature locale (nécessaire pour chiffrer). Aux questions qu'on vous pose, répondez :

* `y` pour signer tous les identifiants
* `0` pour dire que vous ne donnez pas d'indications sur la vérification d'identité que vous avez faite
* `y` pour dire que vous voulez vraiment signer la clé localement