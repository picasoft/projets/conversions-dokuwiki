{{indexmenu_n>20}}
# Administration des machines

Tu trouveras donc dans cette section des explications ou des tutoriels pour la maintenance des machines physiques et virtuelles. :-D

Picasoft possède trois machines physiques : **Alice**, **Bob** et **Caribou**. Alice et Bob sont hébergées chez Tetaneutral à Toulouse et Caribou est hébergée par Rhizome à Compiègne.

<bootnote>Les machines physiques sont aussi appelées **hyperviseurs**.</bootnote>

Un hyperviseur est, en simplifiant, une machine dont la fonction est de faire tourner des machines **virtuelles**, c'est-à-dire des systèmes d'exploitation indépendants et isolés.

Chez Picasoft, aucun service (Mattermost, Etherpad...) ne tourne directement sur les machines physiques : ils tournent tous dans des machines virtuelles, gérées par le système d'exploitation des machines physiques (l'hyperviseur).

Nous utilisons [Proxmox](https://fr.wikipedia.org/wiki/Proxmox_VE) en tant que système d'exploitation principal de nos machines physiques.
{{indexmenu>:technique:infrastructure|js#bitrix msort tsort nsort notoc nomenu nocookie navbar}}