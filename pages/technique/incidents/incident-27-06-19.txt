# 2019/06/27 : onduleur en panne chez Tetaneutral

## Contexte
Un onduleur chez Tetaneutral est tombé en panne à cause de la température. Le souci a été réglé rapidement par Tetaneutral et tout est revenu à la normale.

===== Timeline =====
^  Heure  ^  Événement  ^
|  13h54   | Quentin constate que les services sont down et envoie un message sur la [[https://framateam.org/utc-chaton/channels/town-square | team de secours]] |
|  14h02  | eZag de Tetaneutral est en route vers la salle TLS00 |
|  14h36  | eZag confirme que eaton 28 est en panne |
|  14h46  | Tout est réparé côté Tetaneutral |
|  14h51  | Nos services sont up |

===== Extrait de #tetaneutral.net (freenode) =====
<code>
14∶02
mherrb		aplu_bkp: ce qui est con c'est que la sonde de température est aussi de ce coté. Du coup on ne peut pas mesurer l'effet en live...
aplu_bkp	Tu pense que c'est à cause de la temperature ? genre mpower ou onduleur qui à fait pouf ?
am_		Salut, c'est Andrés de Picasoft. Nos machines (qui sont dans TLS00) ont l'air éteintes... il y a un incident en cours ?
mherrb		zaplu_bkp: pas impossible. C'est déja arrivé y 2 ans (ou 3) qu'un onduleur fasse pouf alors qu'il faisait chaud.
		am_: cf topic...
aplu_bkp	am_: oui, coupure de courant sur une partie des machines (cf topic)
		mherrb: ok, si tu y vas je peux filer un coup de main à partir de 16h30
mherrb		aplu_bkp: eZag est en route.
aplu_bkp	ok :)
14∶36
eZag		Bon c'est bien eaton 28 qui est brulant et qui biip
guilhem[m]	on a du spare ?
eZag		J'ai l'impression meme si le carton est pas neuf
mherrb		eZag: normalement oui celui est est sur l'étagère est neuf.
		eZag: vérifie le disjoncteur sur le le panneau elec. Si celui de droite est down, c'est pas bon .
14∶46
eZag		Okey c'est bon onduleur up
		Et en effet le disjoncteur etait down
		mherrb	log open -autoopen
aplu_bkp	eZag: <3 merci !
blz_rg		ok, merci à tous !!! petit soucis du coté de rg, mais rien à voir avec ttn
</code>
