====== Vérification de la source du mail avec DKIM ======

Avant de trouver un meilleur test, une façon toute simple de vérifier le bon fonctionnement des clés [[technique:old:etudes:mail:spam:blacklisting#dkim|DKIM]] est la suivante :
  root@pica01-test:/DATA/docker/mail/opendkim# opendkim-testkey -d picasoft.net -k nov2018.private -s nov2018 -vvv

qui renvoie :
<code>
opendkim-testkey: key loaded from nov2018.private
opendkim-testkey: checking key 'nov2018._domainkey.picasoft.net'
opendkim-testkey: key not secure
opendkim-testkey: key OK
</code>

Il semblerait que ''opendkim-testkey: key not secure'' ne soit pas une information importante((https://askubuntu.com/questions/134725/setup-dkim-domainkeys-for-ubuntu-postfix-and-mailman et CTRL+F "key not secure".)).

Par ailleurs, il est également possible de récupérer notre clé publique depuis le DNS:
  $dig nov2018._domainkey.picasoft.net TXT +short
qui renvoie:
<code>
"v=DKIM1; h=sha256; k=rsa; " "p=MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0LZPCh8GyDn1loMJs0IDfWVc6hJKnrDIQaUmcaBxtAoZUyEfTBdGIYaqswC/xCMa/ov01QCFP293RD1caN8h45CaArwoiPIjmM0xnq1YmSVRjABBozUVk6o8F08W9XzwO5iolWLfuMA+oiFnIyhH+ATcb8DxBvBTa8AUwCs6fEvfdSfIPlJNHhymWIbg2rWD01klOY36J0OZC1" "p12Fl1fkUU5jNMcJZOezwrqbQqbqZQOF5+1BY/Wx2xWmjdlyn9OR4nE5046pF7l5/yOQr8EPVnrFK3dx2SNiJuOSj1qs56CAINkytvOoZEH65sULwh/HrQjvVvUYQjwYpVl3cOVLGIybo8n6ArsCUMcBiBjBHJRb6vUI7GM9gilZM17wOTDKL6ncxQPlya5DvmQCuf+2Kaz8HZAG/lAYHUqZ2+YaKDcR8k/xz9dQazqNqXSMvLn/DbisUD" "d21vhfSJxEfW4FKQmJ6g9YSJCyzyKuvI8lyiwuXETM/dJI47Y3nlYJjJtIlxPn7MTvPyh5qByr1C3LrjEVFIe2dhA3C105PAkFMCpG7dut8l1nyxBUmJW1qmJ4dR/EHzwVfMnkcZPi/V8+6BUCT2hg95/rUTtwSD1JTTJduuvkAtrLSOaus35p0wsmmqU/glNDF3H9ARqJnAs/5UgEB+zOBs8fZuI/bcxCkCAwEAAQ=="
</code>