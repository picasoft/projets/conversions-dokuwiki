{{indexmenu_n>7}}

# Utiliser le panneau d’administration

## Pour la configuration
Certains paramètres ne sont pas configurables via les variables d'environnement et se configurent donc directement depuis le panneau d'admistration.

<bootnote>Toujours privilégier les variables d'environnement si elles existent, sinon la configuration sera écrasée à chaque redémarrage de conteneur.</bootnote>

### Mail

Le serveur mail se configure via les variables d'environnement `MAIL_URL` et `MAIL_FROM`, mais à ce jour la configuration doit être reportée une seconde fois dans l'interface.
Exemple :

{{ :technique:adminserv:wekan:wekan_mail.png |}}

Le mot de passe n'a pas à être [url-encoded](https://fr.wikipedia.org/wiki/Encodage-pourcent), comme dans les variables d'environnement.
<bootnote warning>La case `TLS` doit restée **décochée**, car elle correspond au protocole SMTPS. Nous utilisons SMTP avec l'extension STARTTLS.</bootnote>

### Interface

Le logo et le texte de l'accueil de Wekan se configure via l'onglet *Interface* :

{{ :technique:adminserv:wekan:wekan_logo.png |}}

L'implémentation est pour le moment assez rustique. Un lien est nécessaire pour le logo, on peut déposer le logo sur le [[technique:tips:sftp|serveur SFTP de Picasoft]] et récupérer ainsi un lien vers `uploads.picasoft.net`.

## Annonce aux utilisateurs

L'onglet `Annonce` permet d'envoyer un message à tous les utilisateurs.

<bootnote warning>Ce message sera affiché à **tous les utilisateurs** lors de leur prochaine connexion. C'est donc à privilégier pour prévenir d'un bug, d'une perte de données, d'une évolution majeure, etc. En revanche, pour prévenir avant une mise à jour, c'est peut être un peu trop.</bootnote>