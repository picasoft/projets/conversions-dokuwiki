# 2019/10/24 : Alice ne répond plus
## Résumé

Le 24 octobre, une mise à jour globale est faite. Alice ne redémarre pas. L'interface vPro ne permet pas de redémarrer Alice qui reste bloquée. Un bénévole de Tetaneutral intervient le lendemain en salle TLS00 et redémarre physiquement la machine. L'ensemble des services remontent sans aucun problème.

## Timeline

^  Heure  ^  Événement  ^
|  **jeudi 24 octobre 2019**  ||
|  09h40  | Rémy met à jours les machines virtuelles ainsi que les machines physiques. La mise à jour se passe bien. |
|  11h05  | Rémy redémarre les machines. Bob remonte, Alice ne répond pas. On suspecte un problème de [mise à jour du noyau incomplète](https://wiki.picasoft.net/doku.php?id=technique:incident-17-05-19), mais des précautions ont été prises pour que ça n'arrive pas. On passe sur la [team de secours](https://framateam.org/utc-chaton/channels/town-square).|
|  11h54  | Ni Rémy ni Quentin n'arrivent à faire fonctionner les commandes d'alimentation distantes via l'interface vPro (en rebondissant sur Bob). Les commandes échouent avec le message `Invalid remote control command`. Le VNC renvoie `Connection closed` pour toute tentative, malgré l'authentification correcte par mot de passe.|
|  12h27  | Andrés constate que les backups de la VM `pica01` - située sur Alice et faisant tourner entre autres Mattermost et Etherpad - échouent depuis cinq jours à cause d'un disque plein. Les sauvegardes horaires des bases de données ne sont pas externalisées, contrairement aux sauvegardes des VM. Le dilemme est de savoir s'il est pertinent de remonter une sauvegarde vieille de cinq jours en attendant.|
|  12h30  | Quentin et Stéphane contactent Tetaneutral pour obtenir de l'aide, sur IRC, Mastodon et par mail. Andrés communique sur les réseaux sociaux. Stéphane met à jour le site pour informer de l'incident.|
|  18h26  | mherrb de Tetaneutral nous informe qu'il peut se rendre sur place le lendemain à 18h30.|
|  22h10  | Andrés parvient à remonter les services sur Bob à partir de la sauvegarde d'il y a cinq jours. Il est décidé de les éteindre pour ne pas créer de confusion et pour éviter les problèmes de fusion de base de données.|
|  **vendredi 25 octobre 2019**  ||
|  18h37  | mherrb arrive en salle TLS00 avec un écran et un clavier. L'écran n'affiche rien, il tente un redémarrage physique.|
|  18h42  | Tout remonte automatiquement (Proxmox, VM, services, vPro, VNC). Fin de l'incident.|

## Détails

Ce n'est pas la première fois que les accès vPro ne permettent pas de redémarrer la machine, ni que VNC ne fonctionne pas (voir le [premier incident documenté](https://wiki.picasoft.net/doku.php?id=technique:incident-10-01-18)).

vPro ne peut pas remplacer un [port type LOM](https://en.wikipedia.org/wiki/Out-of-band_management), et si la machine ne boote pas correctement, et reste "bloquée", empêche de lancer les commandes de redémarrage.

La mise à jour s'était effectivement bien passée, puisque un redémarrage physique a suffit à faire remonter Alice sans intervention supplémentaire. 

mherrb nous a informé que d'autres machines de leur cluster OpenStack/Ceph, dont la carte mère est similaire à celle de nos machines, restent parfois bloquées au démarrage. Dans ce cas, seul un redémarrage physique permet de régler le problème. 

Notons que pour les prochaines fois, on peut avoir des informations en direct sur [la température et l'humidité de la salle TLS00](https://herrb.eu//uthum/).

## Bilan

La communication autour de l'incident a été bonne (mail, réseaux sociaux, sur notre site...), et nos réponses rapides, ce qui a évité de faire craindre aux utilisateurs une perte de données et/ou une indisponibilité de durée indéfinie. L'ensemble de l'équipe s'est retrouvée sur le Framateam de secours, ce qui a permis de se mettre d'accord rapidement.

En revanche, un certain nombre de défauts ont émergé de cet incident :

* Pas de prévention d'une mauvaise mise à jour, ce qui amène à chercher des solutions dans l'urgence
* Pas d'accès physique aux machines, ce qui nous oblige à mobiliser un bénévole de Tetaneutral et à patienter
* Pas de monitoring concernant les backups des VM
* Les backups en mode `Snapshot` provoquent des corruptions des bases de données

Il est donc indispensable, pour les prochaines mises à jour, de prévoir une solution de secours avant la mise à jour. Une idée serait de déclencher un backup manuel des VM de la machine mise à jour (via Proxmox) et d'externaliser ces backups sur l'autre machine (avec une adaptation du script réalisé [pendant cette TX](https://wiki.picasoft.net/doku.php?id=txs:infra-p18:index), afin de pouvoir restaurer immédiatement les VM sans perte de données sur l'autre machine en cas de panne. Ces backups devraient être réalisés en mode `Stop` (https://pve.proxmox.com/wiki/Backup_and_Restore) afin d'éviter toute corruption des bases de données.

Une réflexion sera entamée sur l'accès physique aux machines. Nous allons réfléchir à un moyen de les ramener plus proches de Compiègne, avec toutes les questions que cela implique (quel FAI, quelle qualité de service, pour quel prix, quelle sécurité, etc).

Enfin, une documentation précise concernant les procédures d'accès à distance aux machines (vPro...) et les procédures d'urgence (contact TTNT...) sera rédigée.