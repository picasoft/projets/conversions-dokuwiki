{{indexmenu_n>20}}

## Structure de la base de données

La table `Notes` correspond aux pads dans leurs dernières versions. 

<bootnote warning>L'ID du pad dans l'URL n'est pas dans la même forme que celle sous laquelle il est stocké dans la BDD. En effet, il est codé en base64url dans l'adresse sans les tirets présents dans le champ ID de la BDD.</bootnote>

Ce script permet de transformer l'ID du pad dans l'URL en ID dans la BDD :

<code python>
#!/bin/python3

import base64

i = input()

i = i.replace("_", "/")
i = i.replace("-", "+")
i = i + '=' * (4 - len(i) % 4)
i = base64.b64decode(i).hex()

o = i[0:8] + "-" + i[8:12] + "-" + i[12:16] + "-" + i[16:20] + "-" + i[20:32]

print(o)
</code>

Quant à elle, la table `Revisions` contient les différents patchs à appliquer aux pad pour naviguer entre les versions. 

Enfin la table `Authors` contient l'historique des contributions des auteurs aux notes.