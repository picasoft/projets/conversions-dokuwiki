## Il se passe quoi chez Picasoft ?

Il peut être un peu difficile de se faire une idée de ce qu'il se passe chez Picasoft. Déjà, parce que les membres changent tous les semestres, et ensuite, parce que le fonctionnement de l'asso est un peu _particulier_.

Cette page fait donc un tour d'horizon du fonctionnement et des activités présentes ou passées de l'asso, pour que te donner des idées et rendre tout ça un peu plus concret. :-D

### C'est quoi Picasoft ? Pourquoi ?

Direction la [[asso:communication:presentation|présentation de l'asso]] !

### Fonctionnement de l'asso

Tout est résumé sur la page de la [[asso:fonctionnement:toutdouxcratie|tout-doux-cratie]] !

### Hébergement de services web

Picasoft héberge des services web avec les caractéristiques suivantes :

- Ouverts et gratuits pour **toutes et tous**,
- Respectueux de la vie privée,
- Hébergés en France, sur des serveurs à nous!

Tous les services hébergés sont sur le [site de Picasoft](https://picasoft.net). Cette liste de services s'est étoffée au cours des années et est encore amenée à évoluer. Ils sont et resteront basés sur des **logiciels libres**.

<bootnote>
Les logiciels dits _libres_ sont des logiciels dont la licence et les moyens techniques mis en place permettent à quiconque l'utilisation, l'étude, la modification et la duplication du logiciel. Ces éléments garantissent que nous pouvons garder la main sur ces logiciels et que toute modification qui y sera faite par un tiers pourra être étudiée. Cela permet notamment de s'assurer du respect des droits et de la vie privée des personnes utilisant les services, ainsi que de pouvoir continuer à proposer ces services si les personnes ou organisations en charge de leur développement décident d'arrêter leurs activités.
</bootnote>

<bootnote question>Qui utilise les services ?</bootnote>

Difficile à dire, puisqu'on ne piste pas les utilisateurices ! Mais on sait qu'il y beaucoup d'étudiant·es et de profs, mais aussi des associations d'un peu partout en France, des universités, des petites entreprises... Comme on est référencés sur le site des [CHATONS](https://www.chatons.org/), le collectif dont fait partie Picasoft, n'importe qui peut tomber dessus! Ça marche aussi pas mal par bouche à oreille...

<bootnote question>Et c'est beaucoup utilisé tout ça ?</bootnote>

Tout est relatif... quelques statistiques de mi-2021 pour se faire une idée :

- 1 million de messages sur Mattermost, plus de 7000 comptes et 600 équipes
- 2000 boards sur [Wekan](https://kanban.picasoft.net/) et presque 1000 utilisateurices
- 5000 pads actifs sur [Picapad](https://pad.picasoft.net/) et 1500 sur [CodiMD](https://md.picasoft.net/)
- 4000 fichiers uploadés sur [Picadrop](https://drop.picasoft.net/)

Pour le reste, on sait pas !

<bootnote question>Est-ce-que ça rapporte des grosses thunes ?</bootnote>

Pas du tout, puisque tout est gratuit! Néanmoins on accepte évidemment les dons, et les structures qui utilisent nos services peuvent adhérer à l'association à travers une [adhésion de soutien](https://picasoft.net/co/adherer.html), qu'on publie sur le site.

### Émission de radio

En Mai 2019, on a lancé « La Voix est Libre », une émission de radio <del>pas du tout</del> hebdomadaire pour parler... de ce qu'on veut, tant que c'est à peu près en lien avec les valeurs défendues par Picasoft. ^_^

Arrivés en 2021, on a produit une cinquantaine d'émissions, diffusées en direct sur Graf'hit (94.9FM), sur [graphit.net](https://grafhit.net) et en podcast sur [radio.picasoft.net](https://radio.picasoft.net/co/radio.html).

<bootnote critical>C'est vraiment une super expérience d'aller dans le studio, de choisir la musique, de gérer la console... Et on se limite _a priori_ à aucun thème ni format ! On peut faire des trucs très sérieux comme des trucs parodiques, des interviews, des discussions libres, préparé, à l'arrache... c'est ce qui nous fait plaisir qui compte avant tout.</bootnote>

### Conférences et ateliers

Depuis le lancement de l'asso en 2016, Picasoft a organisé une quinzaine de conférence, soit avec des intervenant·es extérieures, soit concoctées maison ! On peut les retrouver sur [Picatube](https://tube.picasoft.net/video-channels/sensibilisation/videos). Ça peut être sur des thèmes d'actualité, de la vulgarisation au numérique et à la surveillance, des sujets plus « politiques »... Ou des thèmes connexes, comme la monnaie libre.

On organise aussi régulièrement des ateliers pratiques, soit à l'UTC, soit dans des conventions, soit dans des salles de la mairie. 

Il nous arrive aussi d'intervenir dans des établissement scolaires à Compiègne et alentours, auprès d'élèves de collège ou de lycée, même si le contact est moins fort ces derniers semestres.

Quelques exemples d'ateliers qu'on a préparé :

- [[asso:inter:install_party|Install party]], pour mettre Linux sur son PC ou plus généralement pour libérer son PC
- [Contrib'atelier](https://contribateliers.org/), par exemple pour contribuer à OpenStreetMap, le « wikipédia de la cartographie »
- Auto-défense numérique, pour voir en pratique le pistage et s'en protéger (voir [cette trame](https://git.42l.fr/42l/sensibilisation/src/branch/master/atelier-vie-priv%c3%a9e-self-d%c3%a9fense.md) et [celle ci](https://git.42l.fr/42l/sensibilisation/src/branch/master/atelier-cookies-vie-priv%c3%a9e.md) de nos ami·es de 42L, et [notre atelier à distance](https://tube.picasoft.net/videos/watch/888cf20c-28f9-4275-b516-cf2e1d5f0625))
- [Network and Magic](https://code.ffdn.org/ljf/networkandmagic), un jeu de rôle pour comprendre comme marche un réseau
- [Geconomicus](http://geconomicus.glibre.org/), pour comprendre la monnaie libre

Et des fois, on fait des stands rigolos avec des gâteaux. C'est une règle d'or : les gâteaux ça attire toujours les gens.
  
   
{{ :asso:20191128_140731.jpg?600 |}}

### Formation

En plus de la formation technique en interne, Picasoft s'est impliquée dans pas mal d'activités pédagogiques.

#### API

Chaque semestre, les [Activités Pédagogiques à l'Intersemestre](https://apint.utc.fr/), ou API, sont des semaines de formation créditantes à l'UTC. Picasoft en a conçu deux et les anime chaque semestre depuis leur lancement en 2018.

- Init : installation et utilisation de Linux, Git, chiffrement... le tout avec un projet à réaliser en groupe tout au long de la semaine.
- Run : Linux en mode serveur, auto-hébergement d'un site web, Docker...

Le contenu est sur [school.picasoft.net](https://school.picasoft.net/co/school.html).

#### Librecours

Librecours est une plateforme de cours thématiques à distance. Certains cours nécessitent un suivi régulier des apprenant·es, et Picasoft a co-conçu et co-encadré certaines de ces sessions. C'est Stéphane Crozat, enseigner chercheur à Costech et membre de Picasoft, qui gère la plateforme et nous propose d'intervenir.

#### Divers

On est parfois intervenus dans des TD de l'UTC à la demande des enseignant·es, comme par exemple en SI24 pour présenter une panoplie d'outils libres.

#### Git

Pendant plusieurs semestres, on a animé des formations à Git - un logiciel de gestion de version très utilisé en info - en deux parties de 2 heures ([niveau 1](https://gitlab.utc.fr/picasoft/formations/git-v1), [niveau 2](https://gitlab.utc.fr/picasoft/formations/git-v2)).

### Featurings

C'est chouette de préparer des événements avec d'autres associations, et ça permet d'éclairer les problématiques qu'on traite d'une autre manière.

On a déjà fait : 

- Un partenariat avec le Café des Lumières, qui a donné lieu à une séance d'échange et une émission de radio ;
- Un ciné débat avec Cine'mut, avec la projection du film Nothing to Hide ;
- Un article dans le Fil, écrit par une membre

Tout est possible ! :-D

### À l'extérieur...

On se rend quasiment chaque semestre à une convention qui réunit des acteurs du libres, où on peut tenir un stand et faire une présentation. 

<bootnote>L'enjeu est en partie de faire savoir qu'on existe et de sensibiliser au libre, mais bien plus encore de faire collectif avec des copains et copines qui défendent des valeurs similaires ou connexes à celles portées par Picasoft ! C'est avant tout des bons moments où on se rend compte qu'on est pas seul et où on s'éclate. :)</bootnote>

On est notamment allé :

- Aux [Geek Faeries](https://www.geekfaeries.fr/)
- À [Pas Sage en Seine](https://passageenseine.fr/)
- Au [Capitole du Libre](https://capitoledulibre.org/)
- Au [Campus du Libre](https://www.campus-du-libre.org/)
- Aux [Journées du Logiciel Libre](https://www.jdll.org/)
- À l'[Open Source Experience](https://www.opensource-experience.com/) (anciennement POSS - Paris Open Source Summit)

Et chaque année, on participe à la [fête de la science à l'UTC](https://www.utc.fr/fetedelascience/), l'occasion de rencontrer et de sensibiliser un public jeune et des habitant·es du coin.

On est aussi parfois sollicité par des structures pour faire des présentations. Par exemple :

- La mairie du 2e arrondissement de Paris nous a invité pour faire une présentation sur les outils libres et leur raison d'être
- L'association Starting Block nous a proposé de participer au festival Provox, pour un [atelier d'autodéfense numérique avec nos ami·es de 42L](https://www.youtube.com/watch?v=O84AfEQ9S3w).

### CHATONS

En tant que membre du [CHATONS](https://www.chatons.org/), on essaye de participer un minimum à l'animation du collectif. Pour l'instant, on se contente de voter pour l'inclusion de nouvelles structures dans les portées de chatons.

Plus récemment, on a participé au camp CHATONS, un événement regroupant une cinquantaine de personnes du collectif dans un tiers lieu (la Fabrègue) pour réfléchir à la gouvernance du collectif. Toute l'animation du collectif se passe essentiellement sur [le forum](https://forum.chatons.org/), et on aimerait s'y investir un peu plus, lire, relayer ce qu'il se passe, donner notre avis...

### Passer des bons moments

Parce que c'est quand même l'essentiel, non ? Au pic ou ailleurs, faut pas oublier que Picasoft c'est avant tout une bandes de copain·es qui aiment passer du temps ensemble et défendre des trucs qui leur tiennent à cœur. Et que si on se voit pas et qu'on crée pas du lien, on a plus envie de grand chose. 

### Pour tout voir en détail...

<bootnote learn>Les bilans moraux et financiers détaillent chaque semestre l'activité de Picasoft. On peut les retrouver, depuis la création de l'asso, [[asso:administratif:registre|par ici]] !</bootnote>