==== Réutiliser les logos Picasoft avec Inkscape ====

=== Généralités ===
{{ :com:tuto_inkscape_1.png?800 |}}



Le logo principal contient plusieurs couches pour faciliter son éventuelle édition. Pour ceux qui ne sont pas familiers avec cette notion, les couches peuvent être rendues invisibles (bouton œil) ou verrouillées (bouton cadenas) pour éviter de les modifier lorsqu'on travaille avec d'autres couches. On peut rendre visibles certaines couches pour avoir un sous-titre ou un texte gris au lieu d'un texte en couleurs.

----
=== Réutiliser les logos en format SVG ===
Cela permet de modifier plus facilement le logo (changer la couleur du texte, faire un fond circulaire, etc. )

Pour le logo principal, il suffit de tout sélectionner avec Ctrl+Alt+A (vérifier qu'il n'y a pas de couches verrouillées) et coller dans l'affiche à réaliser.

Pour les logos secondaires, chaque logo est dans un "groupe". Il suffit de cliquer sur un des logos pour le sélectionner. 

----
=== Exporter les logos en PNG ===
Si tu utilises un outil de dessin non vectoriel (comme GIMP), tu peux exporter le logo avec Ctrl+Shift+E

Exemple:
----
=== Télécharger ===

Les logos se trouvent sur un dépôt Git : https://gitlab.utc.fr/picasoft/interne/documents/-/tree/master/logos

Autrement, il y a une archive avec les logos principaux :
{{ :com:picasoft_logo_principal.zip |}}

Pour éditer si jamais on modifie le logo:
  * [.svg] Logo principal
Pour les affiches, etc.
  * [.svg] Logo principal avec fond blanc
  * [.svg] Logo principal avec fond noir
  * [.png] Exemple d’export en .png du logo principal

{{ :com:picasoft_logos_secondaires.zip |}}
  * [.svg] Tous les logos secondaires
  * [.png] Logo Picasoft secondaire
  * [.png] Logo Team
  * [.png] Logo Pad