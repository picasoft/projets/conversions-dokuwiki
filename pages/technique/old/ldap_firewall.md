![](indexmenu_n>40)

# Mise en place d\'un pare-feu sur le serveur LDAP

\<bootnote critical\> Depuis que LDAPS est utilisé, le pare-feu a été
retiré. \</bootnote\>

## Motivations

Le serveur LDAP est un **service critique** de l\'infrastructure
Picasoft. En effet, un attaquant qui aurait pris contrôle du LDAP
pourrait facilement se donner les accès à **tout le reste** de
l\'infrastructure.

## Contre-mesure

Pour limiter le risque, on fait du LDAP un service **interne**. En
effet, il n\'a pas vocation a être utilisé depuis l\'extérieur. Pour
cela, on utilisera le service
[iptables](https://fr.wikipedia.org/wiki/Iptables).

Les machines étant autorisées à utiliser le LDAP sont :

-   `pica01-test`
-   `pica01`
-   `pica02`
-   `alice`
-   `bob`

On configure donc iptables sur `monitoring` :

    # iptables -I DOCKER-USER -m multiport -p tcp --dports 389,636 -j DROP
    # iptables -I DOCKER-USER -m multiport -p tcp -s pica01-test.picasoft.net,pica01.picasoft.net,pica02.picasoft.net,monitoring.picasoft.net,alice.picasoft.net,bob.picasoft.net --dports 389,636 -j RETURN

**Explications :**

-   On configure la chaîne `DOCKER-USER` qui est une sous-chaîne de
    `FORWARD` (pour le `NAT` docker)
-   On ajoute une règle qui drop les paquets qui arrivent sur les ports
    `ldap` et `ldaps`
-   Au dessus, on insère une règle qui indique que le serveur répond si
    la requête vient d\'un des serveurs de Picasoft

Notez que les ports utilisés (\`389\`, \`636\`) sont les ports **du
conteneurs** et non pas les ports de l\'hôte. Par exemple, si le
conteneur LDAP mappait le port \`390\` vers le port \`389\`,
\`iptables\` verrait la requête comme si elle était toujours venue du
port \`389\`, car la translation a lieu avant dans les règles
\`iptables\`.
