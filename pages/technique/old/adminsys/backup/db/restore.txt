{{indexmenu_n>20}}

# Restauration d'une sauvegarde

<bootnote warning>Les scripts de restauration sont parfois cassés. Il ne faut pas paniquer et restaurer le backup "à la main".</bootnote>

<bootnote critical>Dans tous les cas, **éteindre le service associé à la base de données!**</bootnote>

## Restauration automatique

<bootnote warning>Cette opération nécessite que le conteneur de base de données soit lancé.</bootnote>
Afin de restaurer une sauvegarde SQL dans l'une des bases de donnée, des scripts on été générés à l'intérieur du [[technique:old:adminsys:backup:db:scripts|conteneur de sauvegarde unique]].

Dans le cas ou ce conteneur s'appelle //pica-backup//, il suffit le lancer les commandes suivantes pour effectuer une restauration :
<code>
# Lancer un shell interactif dans le conteneur
root@machine-hote:~$ docker exec -it pica-backup bash
# Éxecuter le script de restauration dans le conteneur
root@imagedocker:/$ /<service>-restore.sh /backup/<dossier de sauvegarde du service>/<fichier de sauvegarde à restaurer>.tar.gz
</code>

## À la main

Copier le script de restauration dans le conteneur de base de données, exemple pour Etherpad :

```
$ docker cp /DATA/BACKUP/etherpad/<backup> etherpad-db:/tmp
$ docker exec -it etherpad-db bash
```

Utiliser les outils spécifiques au SGBD pour restaurer le backup. Pour PostgreSQL, ce sera une commande du genre :

```
# pg_restore --create --exit-on-error --verbose --dbname=postgres /tmp/<backup>
```

Chaque service est un peu différent (nom d'utilisateur, nom de base...). Dans tous les cas, en tâtonnant on finit toujours par trouver. Remarquez que c'est pas vraiment de la super doc de dire ça, mais en attendant de prendre le temps d'améliorer le système...