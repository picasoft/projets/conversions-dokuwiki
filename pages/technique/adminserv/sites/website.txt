{{indexmenu_n>1}}
# Ajouter un site web sur le domaine Picasoft

## Généralités

Historiquement, nous utilisions une image Nginx maison intégrant PHP afin de publier nos sites. Nous avons décidé de changer pour utiliser les images officielles Nginx et PHP afin d'avoir moins d'images à maintenir ainsi que pour respecter la philosophie Docker:

> Deploy your applications in separate containers independently and in different languages. Reduce the risk of conflict between languages, libraries or frameworks.

Il faut donc créer un nouveau projet sur [le groupe Gitlab dédié aux sites statiques](https://gitlab.utc.fr/picasoft/projets/services/sites-statiques). On pourra se baser sur les exemples existants (`website`, `culture`, `uploads` et `stiegler` par exemple).

Le lancement d'un nouveau site peut [[technique:docker:picasoft:test|s'expérimenter sur la machine de test]].

## Modèle pour un nouveau site « statique »

Un site de base peut être déployé à partir de modèle de fichier Compose.

<bootnote>Dans toutes la suite de l'article, on utilise `monbeauservice` pour nommer le nouveau service : ce mot-clé peut être modifié partout où il est présent pour s'adapter au service réel.</bootnote>

<bootnote learn>Pour comprendre ce qu'est un fichier Compose et ses différentes sections, voir [[technique:tech_team:pres_compose|cette page du guide anti-perdition pour l'équipe technique]].</bootnote>

```yaml
version: "3.7"

volumes:
  monbeausite:
    name: monbeausite

networks:
  proxy:
    external: true

services:
  web:
    container_name: monbeausite
    image: nginx:<version>-alpine
    volumes:
      - monbeausite:/usr/share/nginx/html:ro
    labels:
      traefik.http.routers.monbeausite.entrypoints: websecure
      traefik.http.routers.monbeausite.rule: Host(`monbeausite.picasoft.net`)
      traefik.http.services.monbeausite.loadbalancer.server.port: 80
      traefik.enable: true
    networks:
      - proxy
    restart: unless-stopped
```

<bootnote warning>Remplacer `<version>` par la dernière version de `nginx` sur le [Docker Hub](https://hub.docker.com/_/nginx).</bootnote>

## Personnaliser la configuration

La configuration de nginx se trouve dans le fichier `/etc/nginx/nginx.conf`. Par défaut, l'image Docker `nginx` sert les fichiers du dossier `/usr/share/nginx/html`, n'autorise pas le l'affichage des dossiers et écoute sur le port 80. Cette configuration suffit pour la plupart des cas.

Néanmoins, pour les configurations complexes, il est possible de monter son propre fichier de configuration.

<bootnote web>Plus de détails sur les possibilités de configuration offertes par `nginx` sur le Docker Hub : https://hub.docker.com/_/nginx</bootnote>

On rajoutera alors au fichier Compose présenté plus haut :

```yaml
version: "3.7"
[...]
services:
  web:
    volumes:
[...]
      - ./nginx.conf:/etc/nginx/nginx.conf:ro
[...]
```

## Exemples de personnalisation

<bootnote web>
Pour aller plus loin : 

* [Documentation de référence pour les directives nginx](http://nginx.org/en/docs/)
* [Examples pédagogiques de configuration pour sites statiques](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/)

</bootnote>

### Changer la racine

Par défaut, le contenu est servi depuis `/usr/share/nginx/html`. Or l'emplacement traditionnellement utilisé est `/var/www/html`. Pour cela nous pouvons utiliser la configuration suivante :

```
worker_processes auto;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    server {
        listen 80;

        root /var/www/html;
        index index.html index.htm;
    }
}
```

<bootnote warning>Il faudra bien penser à changer le point de montage du volume dédié au contenu web pour `/var/www/html`.</bootnote>

Ici, la directive à regarder est `root /var/www/html;` indique que le contenu web se trouve dans le dossier `/var/www/html`.

### Activer la compression

La compression permet de réduire la taille des réponses du serveur web, cela permet donc d'améliorer les vitesses de communications. Nous pouvons donc vouloir activer cette compression. La configuration ressemblera alors à la configuration précédente, dans laquelle on modifiera la section `http` :

```
[...]
http {
    [...]
    gzip on;
    gzip_vary on;
    gzip_comp_level 4;
    gzip_min_length 256;
    gzip_types application/pdf application/octet-stream application/atom+xml application/javascript audio/mpeg application/rss+xml image/bmp image/png image/jpeg image/svg+xml image/x-icon text/css text/plain text/html;
    [...]
}
```

La directive `gzip on;` permet d'activer le module de compression. Les autres directives permettent de configurer cette compression (niveau, comportement avec les caches, taille minimum pour activer la compression et types compressés).

<bootnote warning>Si [[technique:tech_team:traefik|Traefik]] est utilisé, l'activation de la compression n'est pas utile car Traefik s'occupe déjà de [compresser les flux](https://gitlab.utc.fr/picasoft/projets/services/traefik/-/blob/3a0455e5772c13e6b92689f08347c1e7d777f0bc/traefik_dynamic.toml#L26).</bootnote>

### Activer l'indexation

Par défaut, l'indexation des fichiers sur le serveur n'est pas activée. Cela pourrait en effet permettre de parcourir les dossiers et éventuellement en cas de mauvaise configuration, d'erreur lors de la publication ou de faille, de découvrir des fichiers qui ne devraient pas être publics. 

Nous pouvons néanmoins vouloir activer cette indexation pour un serveur offrant du contenu culturel par exemple. Dans ce cas nous pouvons modifier la section `server` comme suit :

```
[...]
http {
    [...]
    server {
	autoindex on;
         [...]
    }
}
```

La directive `autoindex on;` permet de dire à nginx, que quand le client demande un dossier, il ne faut pas lui répondre que la demande est interdite mais lui afficher une liste du contenu disponible dans le dossier.

## Configurer PHP

Finalement, il arrive que nous voulions servir des pages dynamiques avec PHP, pour cela il faut ajouter un conteneur qui se chargera d'exécuter le code PHP. 

Ce conteneur doit pouvoir communiquer avec notre serveur web pour recevoir les demande et envoyer le contenu, il faut donc aussi créer un réseau qu'ils partageront. Enfin le conteneur PHP doit pouvoir accéder au code PHP à exécuter, dans ce cas il faut lui donner le contenu web et donc monter les fichiers servis par `nginx`. 

Le fichier Compose dans sa version statique est modifié comme suit :

```yaml
[...]
networks:
  app:
    name: "monbeausite"
    [...]

services:
  app:
    container_name: monbeausite-app
    image: php:8-fpm-alpine
    volumes:
      - monbeausite:/opt/monbeausite:ro
    networks:
      - app
    restart: unless-stopped

  web:
    [...]
    networks:
      [...]
      - app
```

Il faut ensuite indiquer à Nginx qu'il faut contacter le serveur PHP lorsqu'il rencontre un fichier PHP, nous modifions donc le fichier `nginx.conf`.

C'est en particulier les directives `http` et `server` qui seront modifiées :

```
[...]
http {
    [...]
    upstream php-handler {
        server monbeausite-app:9000;
    }

    server {
        [...]
        fastcgi_buffers 64 4K;

        location ~ \.php$ {
            try_files $uri =404;
            fastcgi_param SCRIPT_FILENAME /opt/monbeausite/$fastcgi_script_name;
            include fastcgi_params;
            fastcgi_index index.php;
            fastcgi_pass php-handler;
        }
    }
}
```

La directive `upstream php-handler` définit le serveur qui peut être utilisé pour exécuter le code PHP (il peut en contenir plusieurs). Il contient la directive `server monbeausite-app:9000;`, qui fait référence au __nom du conteneur PHP__ défini dans le nouveau fichier Compose, qui écoute sur le port 9000.

Finalement `location ~ \.php$` indique comment traiter tous les fichiers ayant l'extension `.php`.

<bootnote learn>Pour comprendre en détail la signification de ces directives, tu peux consulter la [configuration de référence pour la communication entre `nginx` et `php`](https://www.nginx.com/resources/wiki/start/topics/examples/phpfcgi) </bootnote>

## Ajouter du contenu

Il suffit d'ajouter les fichiers à servir dans le dossier servi par `nginx`. On l'a vu plus haut, ce dossier est configurable (ce peut être `/usr/share/nginx/html`, `/var/www/html`, etc). 

La commande `docker cp` permet de copier des fichiers locaux dans un conteneur. Par exemple :

```bash
docker cp <dossier_source>/* monbeausite:<dossier_servi>
```

À ce stade, le site est prêt à être servi, il ne reste plus qu'à ajouter l'entrée DNS.

<bootnote web>Pour plus de détail, voir la page [[technique:adminserv:sites:update_from_archive|]].</bootnote>

## Ajout du sous domaine

En filant l'exemple `monbeausite`, il faut ajouter une entrée pour `monbeausite.picasoft.net` dans le fichier de zone DNS.

Pour ce faire, on se référera à la [[technique:adminsys:dns:picasoft|documentation sur la gestion des noms de domaine de Picasoft]], en particulier sur la partie d'ajout de sous-domaine.