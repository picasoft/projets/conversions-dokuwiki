====== Méthodologie ======


==== Conseil et recherche pour les modules ====
Chaque semaine nous avions une réunion avec nos suiveurs. Cette réunion permettait de faire un retour sur le travail fait et de se projeter sur le travail à faire. De plus un contact permanent avec les suiveurs permet de poser des questions et de régler rapidement tout problème de compréhension. L'ordre du jour de chaque réunion était fixé au fur et à mesure dans la semaine. Il était accompagné des points effectués durant la semaine ainsi que le lien des grains de cours modifiés et/ou ajoutés sur le module. Il est important de ne pas oublier de mettre les liens, ils permettent aux suiveurs de se concentrer sur les modifications uniquement (lien vers le pad, également en annexe: [[https://pad.picasoft.net/p/tx-sr10]]).

Avant de commencer chacun des modules, il est judicieux d'effectuer des recherches personnelles afin de cerner le sujet le mieux possible. Il est intéressant de faire remonter les difficultés de compréhension rencontrées, elles seront très utiles pour la confection des exercices. 

Une fois les modules terminés il est intéressant de les faire tester afin d'avoir les retours des étudiants. Ils nous ont permis d'améliorer les modules et d'affiner certaines explications. 


==== Gestion du temps ====
Dans un projet la gestion du temps est l'un des points les plus importants. C'est pourquoi nous avons mis en place dès les premières séances une fiche de temps. Elle a pour but de recenser le travail effectué chaque semaine et le temps passé sur chaque tâche. (lien vers les fiches, également en annexe: [[https://pad.picasoft.net/p/Fiche_temps_Anthony]], [[https://pad.picasoft.net/p/FicheTempLola]])

De plus nous avons mis en place un calendrier afin de se donner des contraintes de temps pour finir les modules. Cela nous a également permis de planifier notre TX et d'évaluer le nombre de modules que nous pourrions faire.  (lien vers le calendrier, également en annexe: {{ :txs:sr10:calendrier.ods |}})