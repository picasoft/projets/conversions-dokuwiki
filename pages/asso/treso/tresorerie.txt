====== Fonction de trésorerie ======
**Pas fini - Work in Progress**

===== Prise de fonction =====
Plusieurs tâches doivent être effectuées avant ou en tout début de semestre. Pour que tout ce déroule correctement, assurer vous que le précédent bureau a fait sa passation (décrite [[asso:treso:tresorerie#fin_de_fonction_passation|en fin de page]]).

==== Avant le début de semestre (intersemestre ou début d'été) ====
**Pas fini - Work in Progress**

===== Fonction =====
**Pas fini - Work in Progress**

  * Tenir à jour les comptes et la caisse de l'association.
  * S'assurer de la gestion saine de Picasoft, en particulier de l'amortissement du matériel et de la prévision des dépenses futures (coût d'hébergement).
  * Gérer les membres de l'association (peut-être délégué à une autre personne du bureau)
    * Réceptionner les cotisations
    * Tenir à jour la liste des membres de l'association
    * Tenir à jour la mailing-list ''picasoft-ag''
  * Mener les réflexions sur le financement de Picasoft et faire les demandes de subventions. **ATTENTION, faire une demande de subvention ou répondre à un appel à projet est chronophage et le reste du bureau (et même de l'association) DOIT assister dans cette tâche.** 

===== Fin de fonction/Passation =====
**Pas fini - Work in Progress**

En fin de mandat, il convient de faire une **bonne passation** au nouveau bureau. Il n'y a rien de plus ennuyeux de devoir chercher des informations lorsque l'on prend ses fonctions, assurer vous d'avoir transmis tout les documents, la caisse et les accès en votre possession au bureau suivant. Si vous ne savez pas comment faire quelque chose ou si vous avez un doute **demandez de l'aide à quelqu'un d'autre dans le bureau, mais en aucun cas n'ignorez une des tâches ci-dessous !**

  * Purger la liste des adhérents de l'association. Toutes les personnes n'ayant pas cotisées pour le semestre suivant doivent être retirées (attention aux personnes ayant cotisées pour 2 semestre donc). La liste actuelle correspond aux personnes abonnées sur la mailing ''picasoft-ag''.
  * Transmettre la caisse, la liste des membres **à jour** et les accès au compte bancaire de Picasoft.