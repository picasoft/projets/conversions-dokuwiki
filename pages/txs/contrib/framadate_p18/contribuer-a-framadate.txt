===== Comment contribuer à un logiciel libre? =====

//Ce mini-guide a pour but de présenter quelques généralités nécessaires pour comprendre la documentation de Framadate//

Les logiciels libres sont réalisés par plusieurs personnes collaborant via internet. Gérer toutes ces contributions venant de divers auteurs n'est pas une tâche facile, c'est pourquoi, dans 99% des cas, le code source est géré par un [[https://fr.wikipedia.org/wiki/Gestion_de_versions|système de gestion de versions]] (ou VCS - Version Control System). L'idée est de conserver l'historique de toutes les modifications, mais aussi de permettre à plusieurs personnes de travailler sur un même projet "sans se marcher dessus". 
De manière simplifiée, pour chaque nouvelle fonctionnalité, les contributeurs créent une nouvelle //branche//. Créer une branche, c'est comme faire une copie du code source: on peut faire autant de modifications que l'on souhaite sans toucher le code source principal. Quand on a fini, on la fusionne (//merge// en anglais) avec la branche principale. Les contributions à Framadate sont un peu plus complexes, plus de détails sur ça plus tard.

{{ :txs:framadate-p18:contrib-framadate-1.png?400 |}}

Le VCS le plus populaire est [[https://git-scm.com/|Git]]. C'est un petit programme qu'on peut installer directement sur notre ordinateur, on peut l'utiliser soit tout seul, soit en combinaison avec GitHub ou GitLab. Quelle est la différence entre ces deux derniers? GitHub est un service, ils hébergent sur leurs serveurs de nombreux projets (open-source ou pas) sous forme de //repositories// Git. La valeur ajoutée par rapport à Git est que GitHub propose aussi la gestion des //issues//, un wiki, des graphiques montrant la fréquence des contributions, etc. GitLab est un service comme GitHub, mais aussi un logiciel libre, et peut donc être installé sur les serveurs d'associations comme Framasoft. L'//instance// GitLab de Framasoft s'appelle Framagit.

Pour apprendre les bases de Git, voici un tutoriel très bien fait qui se fait en 15 min:

[[https://try.github.io|try.github.io]]

==== Contribuer à Framadate ====

Le code source de Framadate est disponible à cette adresse: https://framagit.org/framasoft/framadate
En théorie, la manière de contribuer est la suivante:

{{ :txs:framadate-p18:contrib-framadate-2.png?600 |}}

//Indications sur le site de Framadate://
<code>
1. Créer un compte sur https://framagit.org
2. Créer un fork du projet principal : Créer le fork
3. Créer une branche nommée feature/[Description]
	Où [Description] est une description en anglais très courte de ce qui va être fait
4. Faire des commits dans votre branche
5. Pusher la branche sur votre fork
6. Demander une merge request
</code>
Dans la pratique, les merge requests ne sont pas toujours faites de cette manière, mais il faudrait tout de même respecter cette procédure pour économiser du temps aux developpeurs qui vont relire nos merge requests.

Le meilleur point de partie c'est de relire le [[https://wiki.picasoft.net/doku.php?id=txs:framadate:index|rapport de la TX Framadate A17]]. Il faudrait aussi regarder les comptes framagit des étudiants du semestre précédent:

https://framagit.org/TDavid \\
https://framagit.org/SuperNach0 \\

Et surtout, leurs merge requests:

Erwan: https://framagit.org/framasoft/framadate/merge_requests/194 \\
David: https://framagit.org/framasoft/framadate/merge_requests/206 \\

Ça permettra d'avoir une idée de comment faire, et de pas faire les mêmes erreurs :)
