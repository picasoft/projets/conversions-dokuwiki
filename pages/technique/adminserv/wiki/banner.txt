{{indexmenu_n>5}}
# Ajout d'une bannière

Il est possible de vouloir, temporairement, rajouter une bannière dans le Wiki pour attirer l'attention de l'utilisateur sur un détail particulier. Exemple : changement de méthode d'authentification (compte Dokuwiki vers LDAP).

Un //dirty fix//, suffisant pour les besoins, consiste à modifier le fichier ''/var/www/html/lib/tpl/bootstrap3/topheader.html'', et de dé-commenter le HTML. 

<bootnote>Pour ce faire, il faut d'abord se rendre sur la machine sur laquelle tourne Dokuwiki. On pourra vérifier laquelle à partir des [[technique:graph_services|graphes des services]]. Une fois que l'on a le nom du conteneur, on se rend dedans pour modifier le fichier :

```
$ docker exec -it dokuwiki-app bash
# cd /var/www/html/lib/tpl/bootstrap3
```
</bootnote>

Ce fichier résiste aux mises à jour ; il n'existe pas de base et est injecté juste avant l'ajout de la barre de navigation. Tel que conçu actuellement, il ajoute un gros cadre rouge avec du texte centré. Exemple :

<code html>
<div style="background-color:red; margin-bottom: 10px; text-align:center; padding: 20px; font-size: large;">Ceci est un test.</div>
</code>

Pour toute insulte concernant la présence de CSS dans du HTML, merci de vous adresser au [guignol qui propose ceci](mailto:quentinduchemin@tuta.io).