# Contrats
Contrats et accords impliquant Picasoft.

## Hébergement
Picasoft peut proposer à des associations et/ou particuliers adhérents d'héberger un site web sur son infrastructure.

Un {{:administratif:contrat_hebergement.pdf|modèle de contrat}} d'hébergement est proposé.{{asso:administratif:contrat_hebergement.tex|(Source LaTeX)}}

## Contrats en cours

* {{ :asso:administratif:convention_cet_signee.pdf | Convention CET }}
* {{ :asso:administratif:contrat_caretech-signe.pdf | Contrat CareTech }}
* {{ :asso:administratif:contrat_impactometre-signe.pdf | Contrat Impactomètre }}
* {{ :asso:administratif:contrat_peertube_is_interecoles-signé.pdf | Contrat Peertube - Projet inter-universitaire ingénierie soutenable }}
* {{ :asso:administratif:peertubesc_signe.pdf | Contrat Peertube - Association Scenari }}