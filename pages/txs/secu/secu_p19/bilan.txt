# TX Sécu P19

## Objectifs de la TX

Cette TX fait directement suite à la [TX Sécu A18](https://wiki.picasoft.net/doku.php?id=txs:secu_a18:securite).
Elle est basée sur une reamrque simple : Picasoft sait désormais construire des images docker sécurisées et fonctionelles, avec une intégration continue permettant de protéger les données des utilisateurs.
Néanmois, cela n'a pas d'intérêt si les machines sur lesquelles tournent ces services ne sont pas sécurisées.

Nous avons donc décidé de faire faire un audit des machines, menant à des préconisations et à la mise en place de solutions concrètes pour la sécurisation de l'infra.

Notre journal de bord, rempli tout au long de la TX, est disponible [[jdb|ici]].



## Actions effectuées au cours de la TX :
Au cours de cette TX, nous avons effectuées les actions suivantes :

- Identification des points à étudier.
- Préconisation de limiter les accès root sur les machines.
- Correction de nombreuses permissions erronées au niveau des services.
- Étude des possibilités de restriction du démon docker (voir [[txs:secu:secu_p19:bilan#restriction_de_docker_avec_systemd|plus bas]]).
- Mise en place d'un démon d'audit permettant une traçabilité des actions réalisées sur les machines.
- Centralisation des logs du démon d'audit.
- Initiation à `tmux` des membres présents lors de la soutenance et création d'une [[technique:tips:tmux|page dédiée]] dans le wiki


## Périmètre

L'audit a concerné toutes les VM de l'infrastructure à l'exception de celle de
Stéphane Crozat. Nous avons aussi bien traité les menaces venant de l'extérieur
que de l'intérieur. Pour ces dernières, le but était surtout d'éviter les
accidents, notamment en production, plutôt que de considérer que les membres de
Picasoft aient des intentions malveillantes.

L'audit individuel des services a été fait moins en profondeur, la CI servant à
s'assurer de l'absence de vulnérabilité dans leur déploiement.

Il a cependant été décidé que les machines appartenant aux membres de Picasoft
ne faisaient pas partie du périmètre de l'audit. Ainsi, leur gestion de leurs
clefs SSH et du `pass` relèvent exclusivement de leur responsabilité.

## Permissions

Nous avons observé que tous les membres de l'association avaient les droits
`root` sur l'infrastructure, notamment en production. Cela va à l'encontre du
principe de moindre privilège et facilite les erreurs (`sudo` un peu trop léger,
`poweroff` dans le mauvais terminal, etc.). La raison de cette distribution
large des droits `root` était que n'importe quel utilisateur pouvant contrôler
Docker a indirectement accès aux droits `root` puisqu'il peut notamment modifier
librement le système de fichiers de l'hôte en temps que `root` en le montant
dans un conteneur. Cependant, un tel comportement est suspicieux et n'arrive pas
par accident.

Les droits `root` ont donc été remplacés en production par un ajout au groupe
`docker`, qui est suffisant pour administrer les conteneurs. Certains membres
comme le président et le responsable technique conservent toutefois les droits
`root` pour les tâches d'administration. Les droits additionnels sont ensuite
accordé aux membres en ayant besoin, qui endossent alors les responsabilités
associées. Cette politique n'est pas arrêtée et peut évoluer en fonction des
besoins de l'association et de ses membres ainsi que des problèmes
éventuellement rencontrés après sa mise en place.

## Corrections mineures

### Permissions excessives

Les permissions de nombreux fichiers et répertoires sur tous les hôtes étaient
incorrects pour différentes raisons. Nous avons notamment trouvé des mots de
passe en clair lisible par tout le monde, des fichiers de configuration
inscriptibles par tout le monde et des fichiers exécutables modifiables par les
utilisateurs des services.

Nous les avons corrigées en les restreignant au maximum tout en laissant la
majorité des fichiers de configuration lisibles aux membres de l'association
pour des raisons pédagogiques.

### Suppression de services

Nous avons supprimé plusieurs services abandonnés : une instance n'étant plus
fonctionnelle de Giphy pour Mattermost, un serveur fantôme Minetest présent sur
le serveur de production, et le site inutilisé Wordpress de Compiègne en transition.

### Modifications de configuration

Nous avons modifié les configuration SSH de tous les hôtes afin d'interdire le
forwarding X jusque là permis. Aucun des hôtes ne fait a priori tourner de
serveur X, mais Xorg pourrait éventuellement être un jour installé,
éventuellement en temps que dépendance pour un autre paquet. Xorg étant une
vaste surface d'attaque, nous avons décidé de totalement interdire le forwarding
X.
Nous avons également modifié les droits des journaux générés par `rsyslog` à
`root:sudo`, les administrateurs devant pouvoir y avoir accès appartenant au
groupe `sudo`.


## Restriction de Docker avec systemd

### Motivation

Le contrôle de Docker, accordé par exemple aux utilisateurs du groupe `docker`,
donne indirectement les droits `root` sur le système. Il permet notamment de
monter le système de fichiers de l'hôte et de l'éditer en temps que `root` dans
le conteneur.

On souhaite donc restreindre au maximum les capacités du démon Docker afin de
réduire les menaces provenant des utilisateurs du groupe `docker`. La solution
doit également être suffisamment simple pour être facile à maintenir.

### Principe

Une approche naturelle pour cela est d'employer les capacités de
`systemd.exec(5)`, permettant notamment de réduire les droits d'un service sur
certaines parties du système de fichiers.

On peut par exemple rajouter l'option `ProtectHome = true` à la section
`[Service]` de `/etc/systemd/system/docker.service.d/override.conf` afin
d'apporter des garanties de confidentialité sur `/root`, `/home` et `/run/user`.

### Problème

Depuis la version 18.09 de Docker, containerd n'est plus géré par Docker mais
par systemd. Pour des raisons expliquées dans [cette
issue](https://github.com/moby/moby/issues/38789#issuecomment-466726072), il
n'est donc plus possible d'appliquer d'options systemd de montage à Docker, ce
qui rend la solution proposée impossible à mettre en place.

### Conclusion

Il est impossible de restreindre Docker proprement et efficacement avec systemd.
Nous n'avons pas trouvé d'alternatives pertinentes qui ne soient pas trop
lourdes et intrusives (comme SELinux). De plus, les recommandations officielles
de sécurité concernant Docker indiquent clairement que ce cas est volontairement
ignoré et que l'accès au groupe `docker` ne doit être octroyé qu'aux
utilisateurs auxquels on confierait aussi `root`. Il n'y a donc
vraisemblablement pas de solution officielle à notre besoin.

## auditd

Nous avons mis en place `auditd` sur tous les hôtes de l'infrastructure, avec une centralisation sur `monitoring`. Tous les détails sont disponibles sur la [[technique:old:auditd:howto|page dédiée]], et les informations relatives à la centralisation des logs peuvent êtres trouvées [ici](:centralisation_logs).



## tmux

Nous avons proposé une initiation à `tmux` lors de notre soutenance et avons crée une [[technique:tips:tmux|page dédiée]]. Nous préconisons notamment son utilisation lors des mises à jour, afin qu'une terminaison imprévue de connexion `ssh` ne laisse pas le système dans un état instable.

# Bilan de la TX

Bien que les objectifs de la TX aient évolué (nous n'avons ainsi pas
effectué de tests d'intrusion) en raison des anomalies que nous avons
rencontrées sur l'infrastructure, nous avons avons accompli ce qui nous
a été demandé.

Les solutions techniques mises en place sont solides, maintenables et
documentées et nous avons corrigé tous les problèmes que nous avons
rencontré sur l'infrastructure.

Nous sommes également satisfaits des procédures mises en place. Elles
sont à la fois juste et efficaces, bien qu'elles doivent encore être
acceptées par l'ensemble de l'association.

Pour que notre installation du système de centralisation soit complète,
il ne manque que les certificats nécessaire à l'authentification TLS,
qui seront générés par un membre de l'association.