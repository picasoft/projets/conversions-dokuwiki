{{indexmenu_n>60}}
# Configuration de la délégation DNS

Le nom de domaine ''picasoft.net'' a été réservé par Picasoft chez le //registrar// français [[https://gandi.net|Gandi]]. Généralement le //registrar// propose une interface pour configurer sa zone DNS et utiliser les serveurs DNS du //registrar//. Dans une logique d'auto-hébergement, Picasoft préfère utiliser ses propres serveurs DNS, il est donc nécessaire de déléguer la gestion de la zone DNS des serveurs OVH vers les serveurs de Picasoft.

En pratique, il suffit de se rendre sur le manager Web de Gandi.

<bootnote>Nous n'avons pas de compte Picasoft Gandi : il y a une organisation Picasoft et des comptes individuels autorisés à gérer le domaine `picasoft.net`. Il suffit de demander à un membre d'ajouter votre compte ! :)</bootnote>

Dans les domaines, on sélectionne `picasoft.net` et on accède à la configuration de ce domaine.

<bootnote learn>Pour des raisons de résilience, nous utilisons trois serveurs DNS : deux dans notre infrastructure et un sur le VPS d'un membre. La charge est ainsi répartie entre les serveurs, et si l'un tombe, les autres prennent le relai. Sans serveur DNS fonctionnel, aucun site n'est accessible, c'est donc un point d'attention très important !</bootnote>

{{ technique:adminsys:dns:dns_ns_gandi.png |}}

Ici, on indique les noms de domaine où les serveurs DNS seront accessibles. Par convention, on note `nsNN`, où `ns` tient pour *nameserver* et `N` un entier.

<bootnote question>Comment sait-on où se trouve `ns01.picasoft.net`, puisque c'est justement les serveurs DNS qui traduisent les noms de domaine en IP ?</bootnote>

En effet, on est un peu coincé dans une boucle. C'est le rôle des **glue records** de palier ce souci.

<bootnote learn>Un « glue record » (ou enregistrement Glue) fournit l’adresse IP d’un Serveur de noms de façon à ce que le nom de domaine puisse être résolu dans le cas ou ce dernier utilise des serveurs de noms hébergés sous ce même nom de domaine. *[Source](https://docs.gandi.net/fr/noms_domaine/utilisateurs_avances/glue_records.html)*</bootnote>

Sur Gandi, on créera donc un enregistrement Glue par serveur de nom. Par exemple, `ns01.picasoft.net` pointera vers l'hyperviseur Alice, où on installera un serveur DNS.

{{ technique:adminsys:dns:dns_glue_gandi.png |}}

On fait de même pour tous les serveurs de nom.

Ces modifications peuvent prendre quelques minutes à quelques heures pour être visibles partout.