# 2019/12/05 : coupure chez Tetaneutral

## Contexte
**Coupure de courant non prévue:** un disjoncteur a sauté en salle TLS00, entrainant avec lui les 3 baies hébergeant les machines des adhérent·e·s, nous y compris: https://lists.tetaneutral.net/pipermail/technique/2019-December/003776.html

## Timeline

* Coupure le 5 décembre vers 23h 
* Alimentation rétablie le 6 décembre vers 10h10
* Environ 5 min après, nos services sont up