# Études

Cette catégorie contient les comparatifs et études (faisabilité, popularité...) réalisées par Picasoft, par exemple en vu de l'ouverture de nouveaux services.

{{indexmenu>.#2|js#bitrix msort tsort nsort notoc nomenu nocookie navbar}}