====== Marc : Semaine SU (Modules PostgreSQL) ======
===== Préparation =====
Je me suis rendu à cette séance sans préparation particulière. Je connaissais mes modules sur le bout des doigts puisque je planchais dessus depuis déjà un mois. Je ne savais pas très bien ce que j'allais présenter exactement et comment. J'avais beaucoup réfléchi ces modules pour un apprentissage autonome plus que pour une présentation orale et je me suis vite rendu compte du problème: au tableau, il faut aller à l'essentiel. Si vous faites ce type de présentation à nouveau, n'ayez pas seulement réfléchi à ce que vous pourriez dire: planifiez-le et écrivez-le (au moins les grandes lignes et leur ordre).

===== Déroulement de la journée =====
En 10-15 minutes, je présentais un point particulier autant au niveau théorique qu'au niveau pratique puis les participants pouvaient tenter des manipulations et s'appuyer sur le module écrit pour comprendre. Si quelqu'un avait un résultat inattendu, il n'avait qu'à me demander et on expliquait (avec Stéphane et Andres). Dès qu'on le voulait, on pouvait passer à un nouveau point, l'expliquer et les participants tentaient de nouvelles manipulations.

Très vite, je me suis rendu compte d'une chose: même quand c'est court, les gens ne liront pas vraiment. Les participants lisent en diagonale et se focaliseront sur les commandes. Je comprenais bien quand Stéphane me disait qu'il fallait sectionner et faire plein de manipulations, mais là, ça m'est paru évident qu'il fallait aller bien plus loin que ce que j'avais fait dans cette démarche. Sinon, on peut être vite frustré d'avoir tout synthétisé proprement sans être lu.

La journée a continué comme ceci et plusieurs points sont ressortis : autant des corrections que des ajouts à faire (une sous-partie suivante y sera dédiée). On peut citer par exemple tenter de faire des connexions distantes sur notre serveur. Lorsque j'ai crée le module, j'ai toujours bien réfléchi à des exemples de connexions distantes sans jamais concrètement les tester puisque je travaillais sur ma machine virtuelle. On a cependant vite trouvé la solution grâce à Stéphane.

===== Bilan moral de la journée =====
Ce fut une expérience aussi agréable que dépaysante. D'une part, transmettre un savoir aussi précis est très valorisant en soi. Cependant, dans cette situation, il l'est d'autant plus qu'on a tenté récemment d'acquérir de nouvelles connaissances et on constate que cela a été concrètement utile. Ceci est très rarement le cadre scolaire et universitaire avant les stages TN09 et TN10 (et encore...).

D'autre part, la chose la plus importante que j'ai apprise de la journée :** comment écrire des modules à l'avenir.** J'ai décidé d' attaquer un peu différemment l'écriture de mes modules suivants et on peut le voir avec le dernier module que j'ai réalisé sur le Domain Name System. 
**Dans chaque partie, je me focalise sur une commande que j'essaie d'introduire le plus tôt possible. Puis, j'essaie de rattacher chaque nouveau concept à cette commande. Ainsi, on construit un savoir autour des réactions d'une commande à ce qu'on lui envoie. On comprend mieux autant la commande que les concepts, tout au long du module.** 

===== Bilan technique =====

Outre des petites corrections qui ont déjà été faite, il y a eu plusieurs points d'améliorations du contenu qui sont remontés sur les modules. On a constaté que les grains de contenu suivants seraient à ajouter lors d'une correction complète des modules PostgreSQL:

  * Un grain sur la configuration du postgresql.conf (et surtout sur la "listen_adress" pour permettre des connexions distantes)
  * Un petit grain de manipulation de mot de passe (théoriquement, ça n'apporte pas beaucoup mais dans la pratique c'est nécessaire)
  * Un petit grain avec des schémas pour bien appuyer sur la différence entre l'administrateur Linux et l'administrateur PostgreSQL
  * Un petit exo : création de user, création d'une base dont il n'est pas owner, paramétrer la pg_hba pour qu'il puisse y accéder, s'y connecter en tant que ce nouveau user, constater qu'on ne peut rien y faire. On constatera ainsi une nécessité de gestion des droits de chaque utilisateur dans une base de données donnée. Cette nécessité n'est pas forcément bien comprise de prime abord
  * Un grain (qui pourrait être quasiment un module à part entière) sur l'allumage, le redémarrage,... d'un service comme PostgreSQL. Ceci sera évidemment sur le System D qui est désormais présent dans les distributions Linux les plus répandues.
  * Un exo où on simule une panne et on utilise une sauvegarde pour faire une restauration
  * Si vous êtes très motivés: faire une partie où on relie nos BDD avec un site ou une utilisation concrète d'une base de données (comme cela a pu être fait pendant la semaine SU). Cette utilisation directe de leur SGBD a semblé beaucoup motiver les apprenants à comprendre celui-ci.


====== Alexis : Atelier Docker Basics I ======

===== Préparation =====

Cet atelier a eu lieu le Vendredi 8 Juin, en salle de TD du centre Benjamin Franklin. Nous étions une trentaine à participer à l'atelier ce Vendredi soir.

===== Déroulement de l'atelier =====

Avant d'aller plus loin dans cette partie, il est important de noter que ne pouvant pas encadrer autant de personnes de manière efficace, j'ai réussi à avoir plusieurs étudiants (qui avaient déjà une expérience avec Docker) qui m'ont épaulé dans le déroulement de l'atelier.
Lorsque toute le monde s'était installé, j'ai expliqué comment allait se dérouler la séance : je n'allais pas faire de cours à proprement parler, mais que le but était de tester la réalisation du module en semi-autonomie. En parallèle, chaque participant avait à sa disposition un lien framaform pour capitaliser les retours qu'il avait à faire. 

J'ai mentionné juste avant une //semi-autonomie// car si un participant avait une hésitation sur une notion ou souhaitait aller plus loin, il était possible d'appeler un des encadrant. Cela m'a permit d'identifier rapidement où des petits ajouts (un schéma, une analogie supplémentaire) avaient un réel intérêt pédagogique. 

Le module [[https://school.picasoft.net/modules/dock01b/co/docker_basics.html|Docker Basics]], comme son nom l'indique a pour but de présenter les premières notions clés liées à l'utilisation de Docker, et à prendre cet outil de plus en plus utilisé - mais pas encore enseigné à l'UTC - en main.

===== Bilan moral de l'atelier =====

Ce fut une expérience enrichissante, pour les participants comme pour les encadrants. Personnellement, c'était la première où j'exposais un contenu pédagogique que j'avais créé (presque) de mes propres mains, et voir que l'on arrive à faire passer des notions qui ne nous semblaient pas évidentes au départ est une sensation qui m'a plu. Cet atelier, finalement assez interactif a permit de créer un groupe dynamique, qui pendant 2h avaient envie d'aller le plus loin possible avec Docker. 

Les résultats du framaform sont disponibles ici : {{ :txs:sr-p18:analyse_module_docker_basics.zip |}}

Avant de passer au bilan technique de cet atelier, je souhaite remercier chaleureusement Benjamin M, Benjamin K, Remi et Siyu pour les échanges d'expériences que l'on a eu lors de la séance, mais aussi pour m'avoir permit d'ouvrir l'atelier à plus d'UTCéens. 



===== Bilan technique =====

Négatif :
L'installation. Bien qu'ayant prévenu les participants d'installer Docker sur leur machine au préalable, certains ne l'avaient pas fait, et d'autres avaient encore des soucis à le faire. Bien que les 90% aient commencé à utiliser Docker dans les 10 minutes, nous avons eu des difficultés (Installation des packets, DNS introuvables...) sur certaines distributions Linux, notamment ''Ubuntu 18.04 Bionic Beaver'', version encore récente à la date de l'atelier. Au bout de 45 minutes, les machines les plus réticentes ont fini par courber l'échine et tout le monde pouvait utiliser Docker. Des compléments d'installation ont donc été ajoutés dans la partie installation de Docker Basics.

Positif : Le contenu. Manipuler rapidement a beaucoup plu, et plusieurs m'ont fait remonter qu'ils avaient déjà suivi des cours/tutoriels Docker, mais qu'à l'opposé de cette séance ils n'avaient que peu saisie les notions. Ont-il mieux compris cette fois car ils sont arrivés avec une base, je ne pourrais le dire. Quoi qu'il en soit, le module a été félicité pour sa facilité de compréhension. Le fait qu'il ne s'attarde pas trop sur la théorie, sans pour autant l'oublier a également été apprécié. 

Changements qui ont été intégrés à la version testée : 
    * Ajout d'illustrations pour le principe de container/image et d'image/layers 
    * Bien préciser sur l'install que celle ci est faite pour Debian, et rediriger vers la doc si l'utilisateur est sur une autre distribution
    * Coquilles, reformulation, analogies sur différentes petites partie du module
    * Ajout de compléments (tips, liens, commandes)

Pour la suite : 
  * Rendre disponible la version v1.0 corrigée au public (j'ai eu beaucoup de demandes de publication du module et je ne m'y oppose pas, au contraire!)
