{{indexmenu_n>10}}
## Mobilizon : vive le spam !

<bootnote>On suppose que tu as accès au compte administrateur de [l'instance Mobilizon de Picasoft](https://mobilizon.picasoft.net/). Les identifiants sont sur le [[asso:tuto:vaultwarden]], mais tu peux les demander à quelqu'un de l'équipe de modération.</bootnote>

### Modération fédérée : une autre paire de manches

Mobilizon est un service d'organisation d'événements initialement développé par Framasoft avec pour ambition de proposer une alternative aux événements Facebook. Mobilizon est original par plusieurs aspects : 

- Les membres n'ont pas de profil → on ne peut pas les suivre. L'individualité n'est pas mise au centre de l'outil.
- C'est un service fédéré : plusieurs instances du logiciel forme un réseau → les événements des uns sont visibles depuis les instances des autres.

Ce dernier point est crucial : chaque instance est libre d'appliquer ces propres règles et de se fédérer avec qui elle veut, de sorte que les utilisateurices choisissent l'endroit qui leur convient le mieux.

En revanche, le problème de la modération est double :

- Non seulement n'importe qui peut créer un compte localement et poster du spam, de la pub, etc ;
- Mais les comptes de spam peuvent venir d'autres instances avec lesquelles nous sommes fédérées.

Ce qui rend la modération sur les services fédérés très complexes ; à tel point qu'une [thèse conduit{{:asso:moderation:signalements.png|}}e par une membre](https://fedithese.fr/co/fedithese.html) est en cours.

### Gérer les problèmes locaux

Les signalements sont des demandes de modération par rapport à des contenus __locaux__ mais qui peuvent être faits par des personnes __distantes__ (sur une instance avec laquelle nous sommes fédérés, par exemple).

La liste des signalement est accessible depuis le compte administrateur et ressemble à ça :

{{ :asso:moderation:signalements.png?600 |}}

On constate que ces signalement sont bien effectués par des personnes extérieures. Chaque signalement est transmis sur `picasoft@assos.utc.fr`.

<bootnote warning>Un bug semble empêcher les signalements d'arriver sur les adresses perso des membres qui ont souscrit à `picasoft@assos.utc.fr`. Dans tous les cas, il serait bon de passer de temps en temps pour nettoyer l'instance.</bootnote>

Deux cas se présentent.

##### Compte de spam

Un compte fait uniquement de la publicité et du spam, sur toutes les instances. Exemple typique via un signalement :

{{ :asso:moderation:sign_snow.png?600 |}}

On clique sur l'identité : on constate que cette identité ne publie que du spam.

{{ :asso:moderation:john_profile.png?600 |}}

On décide de supprimer le compte, on y accède au compte en cliquant sur l'email.

<bootnote>Mobilizon permet de gérer plusieurs identités segmentées associées à un même compte, ce qui permet de segmenter ses différentes activités/profils/etc.</bootnote>

{{ :asso:moderation:john_delete.png?600 |}}

On clique sur « Suspendre ».

<bootnote>La suspension du compte rend inaccessible l'ensemble des événements créés, ce qui est plus pratique que de supprimer à la main.</bootnote>

#### Cas d'un contenu litigieux

Ce cas ne s'est jamais présenté mais il serait intéressant de le documenter le cas échéant : un contenu qui pose souci politiquement par exemple. L'association devra alors se positionner sur sa ligne de « laisser-faire ». 

Dans ce cas, il sera peut-être pertinent de supprimer uniquement l'événement, mais pas le compte. La procédure est simple : il faut signaler l'événement, et appuyer sur supprimer dans l'interface de modération. Exemple :

{{ :asso:moderation:delete_event.png?600 |}}

<bootnote>Même pour un·e administrateurice, il n'est pas possible de supprimer un événement depuis sa page dédiée. L'idée est de « forcer » à utiliser les outils de modération, dont on peut garder trace.</bootnote>

### Gérer les problèmes distants

En cas d'événements litigieux sur une instance fédérée, il y a plusieurs solutions :

#### Signaler le problème

En symétrique aux signalements qu'on reçoit : les personnes s'occupant de la modération en face pourront alors supprimer le compte.

#### Suspendre le compte localement

Par exemple, même après avoir suspendu le compte `buyyoutubeviews` en local, on constate qu'il règle un événement dans le flux : 

{{ :asso:moderation:spam_distant.png?600 |}}

En cliquant dessus, on réalise que c'est un compte distant :

{{ :asso:moderation:spam_distant_details.png?600 |}}

On peut rechercher ce compte depuis l'interface de modération :

{{ :asso:moderation:spam_distant_search.png?600 |}}

Et choisir de le suspendre, comme dans le cas local.

<bootnote warning>Attention, il y a un bug : parfois, il faut recharger la page et cliquer plusieurs fois sur « Suspendre ».</bootnote>

<bootnote>Les événements disparaissent alors de la vue locale, même s'il existent toujours sur l'instance distante. Mais au moins, nos utilisateurices ne sont plus exposées au spam, tout en restant connectés aux autres événements distants.</bootnote>

#### Supprimer le lien de fédération

Si une instance distante avec laquelle nous somme fédérés n'a aucune politique de modération et laisse les spammeurs envahir son espace, alors nous en seront victimes en permanence.

Il faut probablement engager le dialogue, et en l'absence de réponse, voter une _défédération_, qui se fait facilement depuis l'interface d'administration.